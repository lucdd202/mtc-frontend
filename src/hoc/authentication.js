import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { selectIsAuthenticated } from "../stores/slices/authenSlice";

const PrivateRoute = ({ component: Component, ...rest }) => {
   const isAuthen = useSelector(selectIsAuthenticated);
   const navigate = useNavigate();
   useEffect(() => {
      if (!isAuthen) {
         navigate("/");
      }
   }, [isAuthen, navigate]);

   return <Component {...rest} />;
};

export { PrivateRoute };
