import "./index.scss";

const BookInfoList = (props) => {
   const { data } = props;
   const infoList = [
      { key: "Author", className: "author" },
      { key: "Status", className: "status" },
      { key: "Genre", className: "genre" },
      { key: "Character", className: "character" },
      { key: "World", className: "world" },
      { key: "Feature", className: "feature" },
      { key: "POV", className: "pov" },
   ];

   return (
      <div className="book-info-list">
         {infoList.map(
            (info) =>
               data?.[info.key]?.value && (
                  <div key={info.key} className={`list-item ${info.className}`}>
                     {data?.[info.key]?.value}
                  </div>
               )
         )}
      </div>
   );
};

export default BookInfoList;
