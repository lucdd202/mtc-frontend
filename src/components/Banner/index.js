import "./index.scss";
import { getRandomBannerService } from "../../services/bannerService";
import { Fragment, useCallback, useEffect, useState } from "react";
import { convertImage } from "../../utils/function";
import { Link } from "react-router-dom";
import { useLocation } from "react-router";

const Banner = (props) => {
   const [randomBanner, setRandomBanner] = useState();
   const { number, style } = props;
   const location = useLocation();

   const handleGetRandomBanner = useCallback(async () => {
      try {
         if (number < 2) {
            const response = await getRandomBannerService(number);
            if (response.status === 200) {
               setRandomBanner(response.data);
            }
         } else {
            const response = await getRandomBannerService(number);
            if (response.status === 200) {
               setRandomBanner(response.data);
            }
         }
      } catch (e) {
         console.log(e);
      }
   }, [number]);

   useEffect(() => {
      handleGetRandomBanner();
   }, [handleGetRandomBanner, location.pathname]);

   if (number < 2) {
      let image = "";
      if (randomBanner?.image) {
         image = convertImage(randomBanner?.image);
      }
      return (
         <Link
            className={`banner random-banner`}
            to={`/truyen/${randomBanner?.bookId}`}
            style={{
               ...style,
               backgroundImage: `url(${image})`,
            }}
         />
      );
   } else {
      return (
         <div className={`banner-container row`}>
            {randomBanner &&
               randomBanner.map((item, index) => {
                  let image = "";
                  if (item?.image) {
                     image = convertImage(item?.image);
                  }

                  return (
                     <Fragment key={index}>
                        <Link
                           className="banner-item"
                           to={`/truyen/${item?.bookId}`}
                           style={{
                              backgroundImage: `url(${image})`,
                           }}
                        />
                        <hr />
                     </Fragment>
                  );
               })}
         </div>
      );
   }
};

export default Banner;
