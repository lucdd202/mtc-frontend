import Footer from "../../components/Footer";
import Header from "../../components/Header";
import {
   ErrorPage,
   HomePage,
   BookPage,
   BookDetailPage,
   InformationPage,
   RankPage,
   AccountPage,
   ProfilePage,
} from "../../pages";
import "./index.scss";
import { Navigate, Route, Routes } from "react-router-dom";
import Banner from "./../Banner/index";

const DefaultLayout = (props) => {
   return (
      <div className="default-layout">
         <Header />

         <Banner number={1} />

         <div className="content-container">
            <Routes path="/">
               <Route path="/" element={<HomePage />} />
               <Route path="/truyen" element={<BookPage />} />
               <Route path="/truyen/:bookId" element={<BookDetailPage />} />
               <Route path="/thong-tin" element={<InformationPage />} />
               <Route
                  path="/bang-xep-hang/:type/:category"
                  element={<RankPage />}
               />
               <Route path="/ho-so/:userId" element={<ProfilePage />} />
               <Route
                  path="/bang-xep-hang/*"
                  element={<Navigate to="/bang-xep-hang/WEEK/VIEW" replace />}
               />
               <Route path="/tai-khoan/*" element={<AccountPage />} />

               <Route path="*" element={<ErrorPage />} />
            </Routes>
         </div>

         <Footer />
      </div>
   );
};

export default DefaultLayout;
