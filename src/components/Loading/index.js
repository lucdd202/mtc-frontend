import "./index.scss";

const Loading = (props) => {
   return (
      <div className="loading">
         <i className="fas fa-spinner"></i>
      </div>
   );
};

export default Loading;
