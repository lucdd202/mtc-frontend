import logo from "../../assets/images/logo.png";
import "./index.scss";

const Footer = () => {
   return (
      <div className="footer-container">
         <img className="logo" alt="logo" src={logo}></img>
         <div className="about mt-3">
            Mê Truyện Chữ là nền tảng mở trực tuyến, miễn phí đọc truyện chữ
            được convert hoặc dịch kỹ lưỡng, do các converter và dịch giả đóng
            góp, rất nhiều truyện hay và nổi bật được cập nhật nhanh nhất với đủ
            các thể loại tiên hiệp, kiếm hiệp, huyền ảo ...
         </div>
      </div>
   );
};

export default Footer;
