import { Fragment, useEffect, useState } from "react";
import "./index.scss";
import { Link } from "react-router-dom";
import Loading from "../Loading";

const ChapterList = (props) => {
   const { data, type } = props;
   const [chapters, setChapters] = useState([]);
   const [sortType, setSortType] = useState(false);
   //false là  tăng dần mà true là giảm dần

   const addDivider = (i) => {
      if (i + 1 > 3 && (i + 1) % 3 === 0) {
         return <hr></hr>;
      }
   };

   useEffect(() => {
      setChapters(data);
   }, [data]);

   const handleSortChapters = (chapters) => {
      setChapters(chapters.reverse());
      setSortType(!sortType);
   };

   return (
      <div className="chapter-list">
         <div className="list-title">
            <h5>Danh sách chương</h5>
            <div
               className="sort-chapter"
               onClick={() => handleSortChapters(chapters)}
            >
               {sortType ? (
                  <i className="fas fa-sort-amount-down"></i>
               ) : (
                  <i className="fas fa-sort-amount-down-alt"></i>
               )}
            </div>
         </div>
         <div className="list-container row  my-4">
            {chapters?.length > 0 ? (
               chapters?.map((item, index) => (
                  <Fragment key={index}>
                     <Link
                        to={`/truyen/${item.bookId}/${item.chapterNumber}`}
                        className={`list-item ${
                           type === "BOOK" ? "col-4" : "col-6"
                        }`}
                     >
                        {`Chương ${
                           item.chapterNumber < 10
                              ? `0${item.chapterNumber}`
                              : item.chapterNumber
                        } : ${item.title} (2 năm trước)`}
                     </Link>
                     {addDivider(index)}
                  </Fragment>
               ))
            ) : (
               <Loading />
            )}
         </div>
      </div>
   );
};

export default ChapterList;
