import { useEffect, useState } from "react";
import "./index.scss";
import { convertImage } from "../../../utils/function";
import { Link } from "react-router-dom";

const RatingCard = (props) => {
   const { data } = props;
   const [avatar, setAvatar] = useState();

   useEffect(() => {
      if (data?.User?.avatar) {
         setAvatar(convertImage(data?.User?.avatar));
      }
   }, [data]);

   return (
      <div className="rating-card">
         <div className="rating-header row">
            <img src={avatar} className="col-2 rating-avatar" alt="" />
            <div className="col-8 rating-info">
               <div className="rating-user">
                  <Link to={`/user/${data?.userId}`}>{data?.User?.name}</Link>{" "}
                  đánh giá
               </div>
               <Link to={`/truyen/${data?.bookId}`} className="rating-book">
                  {data?.Book?.name}
               </Link>
            </div>
            <div className="col-2 rating-point">{data?.point.toFixed(2)}</div>
         </div>
         <div className="rating-content mt-2">{data?.content}</div>
      </div>
   );
};

export default RatingCard;
