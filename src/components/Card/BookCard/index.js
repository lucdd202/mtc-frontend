import "./index.scss";
import { UserOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { convertImage } from "../../../utils/function";

const BookCard = (props) => {
   const { className, data, image, type } = props;
   const [imgURL, setImgURL] = useState("");

   useEffect(() => {
      if (data?.image) {
         setImgURL(convertImage(data?.image));
      }
   }, [data]);

   return (
      <div
         className={`book-card ${className}  ${
            image ? "col-6 row" : "mt-4 px-3"
         }`}
      >
         {image && (
            <Link
               to={`/truyen/${data?.id}`}
               className={type === "rank" ? "left-rank" : `col-3`}
            >
               <img className="card-image" src={imgURL} alt="" />
            </Link>
         )}

         <div
            className={`card-info ${
               image && type === "rank"
                  ? "right-rank"
                  : image && type !== "rank"
                  ? "col-9"
                  : "newest-book-info"
            }`}
         >
            <Link
               to={`/truyen/${data?.id}`}
               className={`title ${!image && "text-center mb-2"}`}
            >
               {data?.name}
            </Link>
            {type === "rated" && (
               <div className="rating-info">
                  <div className="rating-point">
                     {data?.average ? data?.average.toFixed(2) : null}
                  </div>
                  <div className="rating-count">3 đánh giá</div>
               </div>
            )}
            <p
               className={`description ${type === "rank" && "rank"}`}
               dangerouslySetInnerHTML={{
                  __html: data?.description,
               }}
            ></p>

            <div className={`more-info ${type === "rank" ? "rank" : "row"}`}>
               <div className={`author ${type !== "rank" ? "col-7" : ""}`}>
                  <UserOutlined />
                  {data?.BookInfo?.Author?.value}
               </div>
               {type === "rank" && (
                  <div className="count">{data?.Analytics?.count}</div>
               )}
               <div className={`genre ${type !== "rank" ? "col-5" : ""}`}>
                  {data?.BookInfo?.Genre?.value}
               </div>
            </div>
         </div>
      </div>
   );
};

export default BookCard;
