import { rankMenu } from "../../../utils/constant";
import "./index.scss";
import { Link } from "react-router-dom";

const RankDropdown = (props) => {
   return (
      <div className="rank header-item col-6">
         <div className="rank-title">Bảng xếp hạng</div>

         <div className={`rank-menu`}>
            {rankMenu.map((item, index) => (
               <Link to={item.path} className="rank-item" key={index}>
                  {item.label}
               </Link>
            ))}
         </div>
      </div>
   );
};

export default RankDropdown;
