import { Link, useLocation } from "react-router-dom";
import "./index.scss";
import { useSelector } from "react-redux";
import { selectUserId } from "../../../stores/slices/authenSlice";
import {
   checkNotificationService,
   getAllNotificationsService,
} from "../../../services/notificationService";
import { useCallback, useEffect, useState } from "react";

const NotificationDropDown = (props) => {
   const userId = useSelector(selectUserId);
   const [notifications, setNotifications] = useState([]);
   const location = useLocation();

   const handleFetchAllNotification = useCallback(async () => {
      try {
         const response = await getAllNotificationsService(userId);
         if (response && response.status === 200) {
            setNotifications(
               response.data.filter((notification) => !notification.isRead)
            );
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId]);

   const handleCheckNotification = async (idInput) => {
      try {
         const response = await checkNotificationService({ id: idInput });
         if (response && response.status === 200) {
            handleFetchAllNotification(userId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const notificationsShow = (notifications) => {
      let result;
      if (notifications.length > 0) {
         result = notifications.map((notification, index) => (
            <li key={index}>
               <Link
                  to={`/truyen/${notification.bookId}`}
                  onClick={() => handleCheckNotification(notification.id)}
               >
                  {notification.content}
               </Link>
            </li>
         ));
      } else {
         result = (
            <div className="text-center" style={{ fontWeight: 400 }}>
               Không có thông báo nào
            </div>
         );
      }

      return result;
   };

   useEffect(() => {
      handleFetchAllNotification();
   }, [handleFetchAllNotification, location.pathname]);

   return (
      <div className="notification header-item col-2 dropdown-center">
         <div
            type="button"
            className="dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
         >
            <i className="fas fa-bell"></i>
            <span
               className={
                  notifications.length > 0 ? "new-notification text-center" : ""
               }
            >
               {0 < notifications.length &&
                  (notifications.length < 100 ? notifications.length : "99+")}
            </span>
         </div>

         <div className="dropdown-menu dropdown-menu-right">
            <ul className="notification-item">
               {notificationsShow(notifications)}
            </ul>
            <Link to="/tai-khoan/thong-bao" className="notification-item">
               Xem tất cả
            </Link>
         </div>
      </div>
   );
};

export default NotificationDropDown;
