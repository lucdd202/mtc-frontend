import { MenuOutlined } from "@ant-design/icons";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getAllCategoriesService } from "../../../services/categoryService";
import "./index.scss";

const GenreDropdown = (props) => {
   const [genres, setGenres] = useState([]);
   const handleFetchAllGenres = async () => {
      try {
         const response = await getAllCategoriesService("GENRE");
         if (response) {
            setGenres(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllGenres();
   }, []);
   return (
      <div className="genre header-item col-6">
         <div>
            <MenuOutlined /> Thể loại
         </div>
         <div className="genre-menu row">
            <Link to="/truyen" className="genre-item col-6">
               Tất cả
            </Link>
            {genres.map((item, index) => (
               <Link
                  to={`/truyen/?genre=${item.key}`}
                  className="genre-item col-6"
                  key={index}
               >
                  {item.value}
               </Link>
            ))}
         </div>
      </div>
   );
};

export default GenreDropdown;
