import { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./index.scss";
import {
   getUserById,
   selectUserDetail,
} from "../../../stores/slices/userSlice";
import { getUserByIdService } from "../../../services/userService";
import { logout, selectUserId } from "../../../stores/slices/authenSlice";
import { Link } from "react-router-dom";
import { convertImage } from "../../../utils/function";

const ProfileDropdown = (props) => {
   const [imgURL, setImgURL] = useState("");
   const userId = useSelector(selectUserId);
   const userDetail = useSelector(selectUserDetail);
   const dispatch = useDispatch();

   const handleGetUserDetail = useCallback(async () => {
      try {
         const response = await getUserByIdService(userId);
         if (response) {
            dispatch(getUserById(response.data));
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId, dispatch]);

   useEffect(() => {
      handleGetUserDetail();
   }, [handleGetUserDetail]);

   useEffect(() => {
      if (userDetail?.avatar) {
         setImgURL(convertImage(userDetail?.avatar));
      }
   }, [userDetail]);

   return (
      <div className={`user col-5 `}>
         <div
            type="button"
            className="dropdown-toggle"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
         >
            <div
               className="user-avatar"
               style={{ backgroundImage: `url(${imgURL})` }}
            ></div>
            <div className="user-name">{userDetail?.name}</div>
         </div>

         <div className={`user-menu dropdown-menu dropdown-menu-right`}>
            <div className="user-item user-info">
               <div
                  className="user-avatar"
                  style={{ backgroundImage: `url(${imgURL})` }}
               ></div>
               <div>
                  <div className="user-name">{userDetail?.name}</div>
                  <div className="d-flex">
                     <div className="d-flex">
                        <i className="svg-icon icon-flower"></i>
                        <a className="number-flower" href="/#">
                           {userDetail?.flower}
                        </a>
                     </div>
                     <div className="d-flex">
                        <i className="svg-icon icon-candy"></i>
                        <a className="number-candy" href="/#">
                           {userDetail?.candy ?? 0}
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            {userDetail?.roleId === "ROLE0" && (
               <Link to="/admin" className="user-item">
                  Admin
               </Link>
            )}
            <Link to={`/ho-so/${userId}`} className="user-item">
               Hồ sơ
            </Link>
            <Link to="/tai-khoan/tu-truyen" className="user-item">
               Tủ truyện
            </Link>
            <Link to="/tai-khoan/cai-dat" className="user-item">
               Cài đặt
            </Link>
            <Link to="/tai-khoan/mua-keo" className="user-item">
               Mua kẹo
            </Link>
            <hr />
            <div
               to="/"
               className="user-item"
               onClick={() => dispatch(logout())}
            >
               Thoát
            </div>
         </div>
      </div>
   );
};

export default ProfileDropdown;
