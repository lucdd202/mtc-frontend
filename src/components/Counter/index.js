import { useReducer } from "react";
import "./index.scss";

const reducer = (state, action) => {
   const { type, typeConfig, setData, data } = action;
   switch (type) {
      case "INCREMENT":
         return {
            data: setData(data + getValueByType(typeConfig)),
            typeConfig,
         };
      case "DECREMENT":
         return {
            data: setData(data - getValueByType(typeConfig)),
            typeConfig,
         };
      default:
         return state;
   }
};

const getValueByType = (typeConfig) => {
   switch (typeConfig) {
      case "SIZE":
         return 1;
      case "WIDTH":
         return 100;
      case "SPACING":
         return 10;
      default:
         return 0;
   }
};

const Counter = (props) => {
   const { typeConfig, data, setData } = props;
   //SIZE , WIDTH , SPACING

   const [state, dispatch] = useReducer(reducer, { data, typeConfig });

   return (
      <div className="counter">
         <i
            className="fas fa-minus col-2"
            onClick={() =>
               dispatch({ typeConfig, data, setData, type: "DECREMENT" })
            }
         ></i>
         <div className="col-8">
            {data}
            {typeConfig === "SPACING" ? "%" : "px"}
         </div>
         <i
            className="fas fa-plus col-2"
            onClick={() =>
               dispatch({ typeConfig, data, setData, type: "INCREMENT" })
            }
         ></i>
      </div>
   );
};

export default Counter;
