import { SendOutlined } from "@ant-design/icons";
import { useCallback, useEffect, useState } from "react";
import { convertImage } from "../../../utils/function";
import { getUserByIdService } from "../../../services/userService";
import {
   getUserById,
   selectUserDetail,
} from "../../../stores/slices/userSlice";
import { useDispatch, useSelector } from "react-redux";
import { selectUserId } from "../../../stores/slices/authenSlice";
import "./index.scss";

const CommentInput = (props) => {
   const { onClick, value, onChange, type, className } = props;
   const userId = useSelector(selectUserId);
   const [userImg, setUserImg] = useState();
   const userDetail = useSelector(selectUserDetail);
   const dispatch = useDispatch();

   useEffect(() => {
      if (userDetail) {
         setUserImg(convertImage(userDetail?.avatar));
      }
   }, [userDetail]);

   const handleGetUserById = useCallback(async () => {
      try {
         const response = await getUserByIdService(userId);
         if (response.status === 200) {
            dispatch(getUserById(response.data));
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId]);

   useEffect(() => {
      handleGetUserById();
   }, [handleGetUserById]);

   return (
      <div className={`comment-form ${className}`}>
         {" "}
         {type !== "rating" && (
            <img src={userImg} alt="" className="user-image" />
         )}
         <div className="comment-field">
            <textarea
               className="comment-input"
               value={value}
               onChange={(e) => onChange(e.target.value)}
               tabIndex={0}
            />
            <SendOutlined className="comment-send" onClick={onClick} />
         </div>
      </div>
   );
};

export default CommentInput;
