import { useState } from "react";
import "./index.scss";

const PasswordInput = (props) => {
   const { name, className, onChange, value } = props;
   const [isHide, setHide] = useState(true);

   return (
      <div className={`${className} password-input`}>
         <input
            name={name}
            type={isHide ? "password" : "text"}
            onChange={onChange}
            value={value}
         />
         {isHide ? (
            <i className="fas fa-eye" onClick={() => setHide(!isHide)}></i>
         ) : (
            <i
               className="fas fa-eye-slash"
               onClick={() => setHide(!isHide)}
            ></i>
         )}
      </div>
   );
};

export default PasswordInput;
