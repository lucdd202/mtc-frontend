import { Form, Select } from "antd";
import { useEffect, useState } from "react";

const SelectCategory = (props) => {
   const { setData, items, label } = props;
   const [item, setItem] = useState({
      label: "",
      value: "",
   });

   useEffect(() => {
      if (item) {
         setData(item.value);
      }
   }, [item]);

   const filterOption = (input, option) =>
      (option?.label ?? "").toLowerCase().includes(input.toLowerCase());

   return (
      <Form.Item label={label}>
         <Select
            style={{
               width: "100%",
            }}
            placeholder={`Chọn ${label}`}
            value={{
               label: item.label,
               value: item.value,
            }}
            labelInValue
            showSearch
            filterOption={filterOption}
            onChange={(value, option) =>
               setItem((prev) => ({
                  ...prev,
                  label: option.value,
                  value: option.key,
               }))
            }
            options={items}
         />
      </Form.Item>
   );
};

export default SelectCategory;
