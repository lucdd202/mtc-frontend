import { Form, Input, Modal } from "antd";
import "./index.scss";
import { useEffect, useState } from "react";
import {
   addNewBookService,
   updateBookService,
} from "../../../services/bookService";
import { useSelector } from "react-redux";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import UploadImage from "./../../UploadImage/index";
import { getAllCategoriesService } from "../../../services/categoryService";
import { selectUserId } from "../../../stores/slices/authenSlice";
import SelectCategory from "./SelectCategory";

const ModalBook = (props) => {
   const {
      isModalOpen,
      handleCancel,
      handleOk,
      bookDetail,
      type,
      handleFetchAllBooks,
   } = props;

   const userId = useSelector(selectUserId);

   const [book, setBook] = useState({
      id: "",
      name: "",
      image: "",
      description: "",
      authorId: "",
      converterId: userId,
      genreId: "",
      statusId: "",
      povId: "",
      worldId: "",
      featureId: "",
      characterId: "",
   });

   const [currentImage, setCurrentImage] = useState("");
   const { image, ...bookWithoutImage } = book;
   const [imageUpload, setImageUpload] = useState();

   const [categoryData, setCategoryData] = useState([]);

   const genres = categoryData.filter((item) => {
      if (item.type === "GENRE")
         return {
            value: item.key,
            label: item.value,
         };
   });
   const authors = categoryData.filter((item) => {
      if (item.type === "AUTHOR")
         return {
            value: item.key,
            label: item.value,
         };
   });

   const status = categoryData.filter((item) => {
      if (item.type === "STATUS")
         return {
            value: item.key,
            label: item.value,
         };
   });
   const povs = categoryData.filter((item) => {
      if (item.type === "POV")
         return {
            value: item.key,
            label: item.value,
         };
   });
   const worlds = categoryData.filter((item) => {
      if (item.type === "WORLD")
         return {
            value: item.key,
            label: item.value,
         };
   });
   const characters = categoryData.filter((item) => {
      if (item.type === "CHARACTER")
         return {
            value: item.key,
            label: item.value,
         };
   });
   const features = categoryData.filter((item) => {
      if (item.type === "FEATURE")
         return {
            value: item.key,
            label: item.value,
         };
   });

   const handleFetchAllCategories = async () => {
      try {
         const response = await getAllCategoriesService("ALL");
         if (response.status === 200) {
            setCategoryData(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (type && bookDetail) {
         setBook((prevBook) => ({
            ...prevBook,
            id: bookDetail.id,
            name: bookDetail.name,
            image: bookDetail.image,
            description: bookDetail.description,
            authorId: bookDetail.authorId,
            converterId: bookDetail.converterId,
            genreId: bookDetail.genreId,
            statusId: bookDetail.statusId,
            povId: bookDetail.povId,
            worldId: bookDetail.worldId,
            featureId: bookDetail.featureId,
            characterId: bookDetail.characterId,
         }));

         setCurrentImage(image);
      }
   }, [type, bookDetail, image]);

   useEffect(() => {
      if (!isModalOpen) {
         setBook((prevBook) => ({
            ...prevBook,
            id: "",
            name: "",
            image: "",
            description: "",
            authorId: "",
            converterId: userId,
            genreId: "",
            statusId: "",
            povId: "",
            worldId: "",
            featureId: "",
            characterId: "",
         }));
      }
   }, [isModalOpen]);

   const handleAddNewBook = async () => {
      try {
         const response = await addNewBookService({
            ...book,
            image: imageUpload,
         });

         if (response.status === 200) {
            handleFetchAllBooks();
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllCategories();
   }, []);

   const handleEditBook = async () => {
      try {
         let response = await updateBookService({
            ...book,
            image: imageUpload,
         });

         if (response.status === 200) {
            handleFetchAllBooks();
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleOnChange = (e) => {
      const { name, value } = e.target;
      setBook((prevBook) => ({
         ...prevBook,
         [name]: value,
      }));
   };

   return (
      <Modal
         width={1000}
         title={type ? "Update Book" : " Add New Book"}
         open={isModalOpen}
         onOk={() => {
            if (type) {
               handleEditBook();
            } else {
               handleAddNewBook();
            }

            handleOk();
         }}
         onCancel={handleCancel}
      >
         <Form className="row">
            <div className="col-6">
               <Form.Item
                  label="Tên truyện"
                  rules={[
                     {
                        required: true,
                        message: "Please input book name!",
                     },
                  ]}
               >
                  <Input
                     name="name"
                     value={book.name}
                     onChange={handleOnChange}
                  />
               </Form.Item>
               <SelectCategory
                  items={genres}
                  label="Thể loại"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, genreId: data }))
                  }
               />
               <SelectCategory
                  items={authors}
                  label="Tác giả"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, authorId: data }))
                  }
               />
               <SelectCategory
                  items={status}
                  label="Tình trạng"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, statusId: data }))
                  }
               />
               <SelectCategory
                  items={features}
                  label="Lưu phái"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, featureId: data }))
                  }
               />
               <SelectCategory
                  items={worlds}
                  label="Bối cảnh thế giới"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, worldId: data }))
                  }
               />
               <SelectCategory
                  items={characters}
                  label="Tính cách nhân vật"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, characterId: data }))
                  }
               />
               <SelectCategory
                  items={povs}
                  label="Thị giác"
                  setData={(data) =>
                     setBook((prev) => ({ ...prev, povId: data }))
                  }
               />

               <div className="input-container max-width-input">
                  <label>Tải ảnh lên</label>
                  <UploadImage
                     inputImage={book.image ?? ""}
                     onUploadImage={(image) => setImageUpload(image)}
                  />
               </div>
            </div>
            <div className="col-6">
               <h6>Giới thiệu</h6>
               <CKEditor
                  editor={ClassicEditor}
                  data={book.description ?? ""}
                  onChange={(event, editor) => {
                     const data = editor.getData();
                     setBook((prev) => ({ ...prev, description: data }));
                  }}
               />
            </div>
         </Form>
      </Modal>
   );
};

export default ModalBook;
