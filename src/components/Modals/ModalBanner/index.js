import { Modal } from "antd";
import "./index.scss";
import { useEffect, useState } from "react";
import {
   addNewBannerService,
   updateBannerService,
} from "../../../services/bannerService";
import UploadImage from "../../UploadImage";
import { getAllBooksService } from "../../../services/bookService";

const ModalBanner = (props) => {
   const {
      isModalOpen,
      handleCancel,
      handleOk,
      bannerDetail,
      type,
      bookId,
      handleFetchAllBanners,
   } = props;
   const [banner, setBanner] = useState({
      id: "",
      bookId: "",
      image: "",
   });
   const [books, setBooks] = useState([]);
   const [imageUpload, setImageUpload] = useState();

   useEffect(() => {
      setBanner((prev) => ({
         ...prev,
         bookId: bookId,
      }));
   }, [bookId]);

   const handleFetchAllBooks = async () => {
      try {
         const response = await getAllBooksService();
         if (response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllBooks();
   }, []);

   useEffect(() => {
      if (type && bannerDetail) {
         setBanner((prevBanner) => ({
            ...prevBanner,
            id: bannerDetail.id,
            image: bannerDetail.image,
         }));
      }
   }, [type, bannerDetail]);

   useEffect(() => {
      if (!isModalOpen) {
         setBanner((prevBanner) => ({
            ...prevBanner,
            id: "",
            bookId: "",
            image: "",
         }));
      }
   }, [isModalOpen]);

   const handleAddNewBanner = async () => {
      try {
         const response = await addNewBannerService({
            ...banner,
            image: imageUpload,
         });
         if (response.status === 200) {
            handleFetchAllBanners(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleEditBanner = async () => {
      try {
         let response = await updateBannerService({
            ...banner,
            image: imageUpload,
         });
         if (response.status === 200) {
            handleFetchAllBanners(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <Modal
         width={1000}
         title={type ? "Cập nhật lại chương" : "Thêm chương mới"}
         open={isModalOpen}
         onOk={() => {
            if (type) {
               handleEditBanner();
            } else {
               handleAddNewBanner();
            }

            handleOk();
         }}
         onCancel={handleCancel}
      >
         <select
            value={banner.bookId ?? books[0].id}
            onChange={(e) => setBanner((prev) => ({ bookId: e.target.value }))}
         >
            {books.map((item, index) => (
               <option key={index} value={item.id}>
                  {item.name}
               </option>
            ))}
         </select>

         <div className="input-container max-width-input">
            <label>Tải ảnh lên</label>
            <UploadImage
               inputImage={banner.image ?? ""}
               onUploadImage={(image) => setImageUpload(image)}
            />
         </div>
      </Modal>
   );
};

export default ModalBanner;
