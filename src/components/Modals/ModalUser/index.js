import { Form, Input, Modal, Select } from "antd";
import "./index.scss";
import { useEffect, useState } from "react";
import {
   addNewUserService,
   updateUserService,
} from "../../../services/userService";
import UploadImage from "./../../UploadImage/index";
import { getAllCategoriesService } from "../../../services/categoryService";

const ModalUser = (props) => {
   const {
      isModalOpen,
      handleCancel,
      handleOk,
      userDetail,
      type,
      handleFetchAllUsers,
   } = props;
   const [imageUpload, setImageUpload] = useState();
   const [currentAvatar, setCurrentAvatar] = useState();
   const [roles, setRoles] = useState([]);
   const rolesOption = roles.map((item) => ({
      value: item.key,
      label: item.value,
   }));

   const [user, setUser] = useState({
      name: "",
      password: "",
      email: "",
      roleId: rolesOption[0]?.value,
      avatar: "",
   });
   const { avatar, ...userWithoutAvatar } = user;

   const handleFetchAllRoles = async () => {
      try {
         const response = await getAllCategoriesService("ROLE");
         if (response.status === 200) {
            setRoles(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllRoles();
   }, []);

   useEffect(() => {
      if (type && userDetail) {
         setUser((prevUser) => ({
            ...prevUser,
            name: userDetail.name,
            email: userDetail.email,
            roleId: userDetail.roleId,
            avatar: userDetail.avatar,
         }));
         setCurrentAvatar(avatar);
      }
   }, [type, userDetail, avatar]);

   useEffect(() => {
      if (!isModalOpen) {
         setUser((prevUser) => ({
            ...prevUser,
            name: "",
            email: "",
            roleId: "",
            avatar: "",
         }));
      }
   }, [isModalOpen]);

   const handleAddNewUser = async () => {
      try {
         const response = await addNewUserService({
            ...user,
            avatar: imageUpload,
         });

         if (response.status === 200) {
            handleFetchAllUsers();
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleEditUser = async () => {
      try {
         const response = await updateUserService({
            ...user,
            avatar: imageUpload,
         });

         if (response.status === 200) {
            handleFetchAllUsers();
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleOnChange = (e) => {
      const { name, value } = e.target;
      setUser((prevUser) => ({
         ...prevUser,
         [name]: value,
      }));
   };

   return (
      <Modal
         title={type ? "Update User" : " Add New User"}
         open={isModalOpen}
         onOk={() => {
            if (type) {
               handleEditUser();
            } else {
               handleAddNewUser();
            }

            handleOk();
         }}
         onCancel={handleCancel}
      >
         <Form>
            <Form.Item
               label="Email"
               rules={[
                  {
                     required: true,
                     message: "Please input your email!",
                  },
               ]}
            >
               <Input
                  name="email"
                  placeholder="email"
                  value={user.email}
                  onChange={handleOnChange}
               />
            </Form.Item>

            <Form.Item
               label="Password"
               rules={[
                  {
                     required: true,
                     message: "Please input your password!",
                  },
               ]}
            >
               <Input.Password
                  name="password"
                  placeholder="Password"
                  value={user.password}
                  onChange={handleOnChange}
                  disabled={type}
               />
            </Form.Item>

            <Form.Item
               label="Tên"
               rules={[
                  {
                     required: true,
                     message: "Please input your name!",
                  },
               ]}
            >
               <Input
                  name="name"
                  placeholder="Name"
                  value={user.name}
                  onChange={handleOnChange}
               />
            </Form.Item>

            <Form.Item
               label="Vai trò"
               rules={[
                  {
                     required: true,
                     message: "Please input your name!",
                  },
               ]}
            >
               <Select
                  name="roleId"
                  style={{
                     width: "100%",
                  }}
                  placeholder="Chọn vai trò"
                  value={user.roleId}
                  onChange={(value) =>
                     setUser((prev) => ({ ...prev, roleId: value }))
                  }
                  options={rolesOption}
               />
            </Form.Item>

            <div className="input-container max-width-input">
               <label>Tải ảnh lên</label>
               <UploadImage
                  inputImage={user.avatar ?? ""}
                  onUploadImage={(image) => setImageUpload(image)}
               />
            </div>
         </Form>
      </Modal>
   );
};

export default ModalUser;
