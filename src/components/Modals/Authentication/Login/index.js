import { Checkbox, Form, Input } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";

const Login = (props) => {
   const { user, handleOnChange, handleLogin } = props;

   return (
      <Form
         className="login-form col-8"
         layout="vertical"
         //  wrapperCol={{ span: 16 }}
      >
         <Form.Item
            label="Email"
            rules={[
               {
                  required: true,
                  message: "Please input your username!",
               },
            ]}
         >
            <Input
               className="input-field"
               name="email"
               placeholder="Nhập email"
               value={user.email}
               onChange={handleOnChange}
            />
         </Form.Item>

         <Form.Item
            label="Mật khẩu"
            rules={[
               {
                  required: true,
                  message: "Please input your password!",
               },
            ]}
         >
            <Input.Password
               name="password"
               className="input-field"
               placeholder="Nhập mật khẩu"
               value={user.password}
               onChange={handleOnChange}
               iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
               }
            />
         </Form.Item>

         <Form.Item name="remember" valuePropName="checked">
            <Checkbox className="remember">Ghi nhớ mặt khẩu</Checkbox>
         </Form.Item>

         <button
            className="btn-submit col-12"
            htmltype="submit"
            onClick={handleLogin}
         >
            Đăng nhập
         </button>
      </Form>
   );
};

export default Login;
