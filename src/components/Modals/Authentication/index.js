import { useDispatch } from "react-redux";
import "./index.scss";
import { useState } from "react";
import { loginService, registerService } from "../../../services/authenService";
import { Modal } from "antd";
import Login from "./Login";
import Register from "./Register";
import { login } from "../../../stores/slices/authenSlice";

const Authentication = (props) => {
   const { isModalOpen, handleCancel, handleOk, type, setType } = props;

   const [user, setUser] = useState({
      email: "",
      password: "",
      retype: "",
   });

   const dispatch = useDispatch();

   const handleLogin = async () => {
      try {
         const response = await loginService(user);
         if (response.status === 200) {
            dispatch(login(response.data));
         }
         handleOk();
      } catch (e) {
         console.log(e);
      }
   };

   const handleRegister = async () => {
      try {
         if (user.password === user.retype) {
            const response = await registerService({
               email: user.email,
               password: user.password,
            });

            if (response.status === 200) {
               handleOk();
            }
         } else {
            console.log("Mật khẩu không khớp !");
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleOnChange = (e) => {
      const { name, value } = e.target;
      setUser((prev) => ({
         ...prev,
         [name]: value,
      }));
   };

   return (
      <Modal
         title=""
         open={isModalOpen}
         onOk={handleOk}
         onCancel={handleCancel}
         footer={null}
         className="modal-authen"
      >
         <div className="tab-container">
            <div
               className={`${type ? "" : "active"} tab-item `}
               onClick={() => setType(false)}
            >
               Đăng nhập
            </div>
            <div
               className={`${type ? "active" : ""} tab-item`}
               onClick={() => setType(true)}
            >
               Đăng ký
            </div>
         </div>
         <div className="form-container row">
            {!type ? (
               <>
                  <Login
                     user={user}
                     handleOnChange={handleOnChange}
                     handleLogin={handleLogin}
                  />
                  <div className="mt-4" style={{ textAlign: "center" }}>
                     Bạn chưa có tài khoản ?
                     <span
                        className="signup mx-2"
                        onClick={() => setType(true)}
                     >
                        Đăng ký ngay
                     </span>
                  </div>
               </>
            ) : (
               <Register
                  user={user}
                  handleOnChange={handleOnChange}
                  handleRegister={handleRegister}
               />
            )}
         </div>
      </Modal>
   );
};

export default Authentication;
