import { Form, Input } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from "@ant-design/icons";

const Register = (props) => {
   const { user, handleOnChange, handleRegister } = props;

   return (
      <Form className="register-form col-8" layout="vertical">
         <Form.Item
            label="Email"
            rules={[
               {
                  required: true,
                  message: "Please input your username!",
               },
            ]}
         >
            <Input
               name="email"
               className="input-field"
               placeholder="Nhập email"
               value={user.email}
               onChange={handleOnChange}
            />
         </Form.Item>

         <Form.Item
            label="Mật khẩu"
            rules={[
               {
                  required: true,
                  message: "Please input your password!",
               },
            ]}
         >
            <Input.Password
               name="password"
               className="input-field"
               placeholder="Nhập mật khẩu"
               value={user.password}
               onChange={handleOnChange}
               iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
               }
            />
         </Form.Item>

         <Form.Item
            label="Nhập lại mật khẩu"
            rules={[
               {
                  required: true,
                  message: "Please input your password!",
               },
            ]}
         >
            <Input.Password
               name="retype"
               className="input-field"
               placeholder="Nhập lại Mật khẩu"
               value={user.retype}
               onChange={handleOnChange}
               iconRender={(visible) =>
                  visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
               }
            />
         </Form.Item>

         <button
            className="btn-submit col-12"
            htmltype="submit"
            onClick={handleRegister}
         >
            Đăng ký
         </button>
      </Form>
   );
};

export default Register;
