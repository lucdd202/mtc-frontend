import { Form, Input, Modal } from "antd";
import "./index.scss";
import { useEffect, useState } from "react";
import {
   addNewChapterService,
   updateChapterService,
} from "../../../services/chapterService";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";

const ModalChapter = (props) => {
   const {
      isModalOpen,
      handleCancel,
      handleOk,
      chapterDetail,
      type,
      bookId,
      handleFetchAllChapters,
   } = props;

   const [chapter, setChapter] = useState({
      id: "",
      title: "",
      bookId: "",
      content: "",
      chapterNumber: "",
   });

   useEffect(() => {
      setChapter((prev) => ({
         ...prev,
         bookId: bookId,
      }));
   }, [bookId]);

   useEffect(() => {
      if (type && chapterDetail) {
         setChapter((prevChapter) => ({
            ...prevChapter,
            id: chapterDetail.id,
            title: chapterDetail.title,
            content: chapterDetail.content,
            chapterNumber: chapterDetail.chapterNumber,
         }));
      }
   }, [type, chapterDetail]);

   useEffect(() => {
      if (!isModalOpen) {
         setChapter((prevChapter) => ({
            ...prevChapter,
            id: "",
            title: "",
            bookId: "",
            content: "",
            chapterNumber: "",
         }));
      }
   }, [isModalOpen]);

   const handleAddNewChapter = async () => {
      try {
         const response = await addNewChapterService(chapter);
         if (response.status === 200) {
            handleFetchAllChapters(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleEditChapter = async () => {
      try {
         let response = await updateChapterService(chapter);
         if (response.status === 200) {
            handleFetchAllChapters(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleOnChange = (e) => {
      const { name, value } = e.target;
      setChapter((prevChapter) => ({
         ...prevChapter,
         [name]: value,
      }));
   };

   return (
      <Modal
         width={1000}
         title={type ? "Cập nhật lại chương" : "Thêm chương mới"}
         open={isModalOpen}
         onOk={() => {
            if (type) {
               handleEditChapter();
            } else {
               handleAddNewChapter();
            }

            handleOk();
         }}
         onCancel={handleCancel}
      >
         <Form className="row">
            <div className="col-6">
               <Form.Item
                  label="Tiêu đề"
                  rules={[
                     {
                        required: true,
                        message: "Please input chapter name!",
                     },
                  ]}
               >
                  <Input
                     name="title"
                     placeholder="Nhập tiêu đề"
                     value={chapter.title}
                     onChange={handleOnChange}
                  />
               </Form.Item>
               <Form.Item
                  label="Số chương"
                  rules={[
                     {
                        required: true,
                        message: "Please input chapter number!",
                     },
                  ]}
               >
                  <Input
                     name="chapterNumber"
                     placeholder="Nhập số chương"
                     value={chapter.chapterNumber}
                     onChange={handleOnChange}
                  />
               </Form.Item>
            </div>
            <div className="col-6">
               <h6>Nội dung</h6>
               <CKEditor
                  editor={ClassicEditor}
                  data={chapter.content ?? ""}
                  onChange={(event, editor) => {
                     const data = editor.getData();
                     setChapter((prev) => ({ ...prev, content: data }));
                  }}
               />
            </div>
         </Form>
      </Modal>
   );
};

export default ModalChapter;
