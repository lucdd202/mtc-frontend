import { useEffect, useState } from "react";
import {
   deleteCommentService,
   getCommentRepliesService,
   replyCommentService,
} from "../../services/commentService";
import { convertImage, convertTime, showStar } from "../../utils/function";
import { useSelector } from "react-redux";
import { selectUserId } from "../../stores/slices/authenSlice";
import CommentInput from "../Input/Comment";
import "./index.scss";
import {
   deleteRatingService,
   getRatingRepliesService,
   replyRatingService,
} from "../../services/ratingService";
import Loading from "../Loading";

const Comment = (props) => {
   const { data, replyToId, fetchAllData, type } = props;
   const userId = useSelector(selectUserId);
   const [isReply, setIsReply] = useState(false);
   const [repliesData, setRepliesData] = useState([]);

   const [replyField, setReplyField] = useState({
      bookId: "",
      userId: "",
      content: "",
      replyToId: "",
   });

   useEffect(() => {
      if (userId && replyToId && data) {
         setReplyField((prev) => ({
            ...prev,
            bookId: data.bookId,
            userId: userId,
            replyToId: replyToId,
         }));
      }
   }, [userId, replyToId, data]);

   const handleDeleteComment = async (data) => {
      try {
         let response;
         if (type === "rating") {
            response = await deleteRatingService(data.id);
         } else {
            response = await deleteCommentService(data.id);
         }
         if (response.status === 200) {
            fetchAllData(data.replyToId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleFetchAllReplies = async (replyToId) => {
      try {
         let response;
         if (type === "rating") {
            response = await getRatingRepliesService(replyToId);
         } else {
            response = await getCommentRepliesService(replyToId);
         }

         if (response.status === 200) {
            setRepliesData(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleSendReply = async (sendData) => {
      try {
         let response;
         if (type === "rating") {
            response = await replyRatingService(sendData);
         } else {
            response = await replyCommentService(sendData);
         }

         if (response.status === 200) {
            handleFetchAllReplies(sendData.replyToId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const btnSendClicked = (dataSend) => {
      handleSendReply(dataSend);
      setReplyField((prev) => ({
         ...prev,
         content: "",
      }));
   };

   const [image, setImage] = useState();

   useEffect(() => {
      if (data?.User?.avatar) {
         setImage(convertImage(data?.User?.avatar));
      }
   }, [data]);

   return (
      <div className="comment-container ">
         <div className="comment-item">
            <img className="comment-avatar" src={image} alt="" />
            <div className="comment-main">
               <div className="comment-name">
                  <div>{data?.User?.name}</div>
                  <i
                     className="fas fa-trash comment-delete"
                     onClick={() => handleDeleteComment(data)}
                  ></i>
               </div>
               <div className="comment-time">
                  {data?.point && (
                     <div>
                        {showStar(data?.point)}
                        {data?.point}
                     </div>
                  )}

                  <div>
                     <i className="far fa-clock"></i>
                     {convertTime(data.createdAt)}
                  </div>

                  {replyToId && data.commentAt && (
                     <div>
                        <i className="fas fa-glasses"></i>Chương{" "}
                        {data?.commentAt}
                     </div>
                  )}
               </div>

               <div className="comment-content">{data.content}</div>
               <div className="comment-action">
                  <div className="left-action">
                     {!isReply && data?.Comments?.length > 0 && (
                        <div
                           className="has-replies"
                           onClick={() => {
                              setIsReply(!isReply);
                              handleFetchAllReplies(data.id);
                           }}
                        >
                           Xem {data?.Comments?.length} trả lời
                        </div>
                     )}
                  </div>
                  <div className="right-action">
                     <div>
                        <i className="fas fa-thumbs-up"></i>0
                     </div>
                     {replyToId && (
                        <div
                           onClick={() => {
                              handleFetchAllReplies(replyToId);
                              setIsReply(!isReply);
                           }}
                        >
                           <i className="fas fa-reply"></i>
                           Trả lời
                        </div>
                     )}
                     <div>
                        <i className="fas fa-flag"></i>
                        Báo xấu
                     </div>
                  </div>
               </div>
               {isReply &&
                  (repliesData.length > 0 ? (
                     <>
                        <hr />
                        {repliesData.map((item, index) => (
                           <Comment
                              key={index}
                              data={item}
                              fetchAllData={handleFetchAllReplies}
                              type={type}
                           />
                        ))}
                        <CommentInput
                           value={replyField.content}
                           onChange={(value) =>
                              setReplyField((prev) => ({
                                 ...prev,
                                 content: value,
                              }))
                           }
                           onClick={() => btnSendClicked(replyField)}
                        />
                     </>
                  ) : (
                     <Loading />
                  ))}
            </div>
         </div>
         <hr />
      </div>
   );
};

export default Comment;
