import React, { useState } from "react";
import "./index.scss";
import { useNavigate } from "react-router";

const SearchBar = (props) => {
   const navigate = useNavigate();
   const [searchInput, setSearchInput] = useState("");

   const handleSearchBook = (value) => {
      navigate(`/truyen/?keyword=${value}`);
   };

   return (
      <div className="search-container">
         <input
            className="search-input"
            placeholder="Tìm kiếm"
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
            onKeyDown={(e) => {
               if (e.key === "Enter") {
                  handleSearchBook(searchInput);
               }
            }}
         />
         <i
            className="fas fa-search"
            onClick={() => handleSearchBook(searchInput)}
         />
      </div>
   );
};
export default SearchBar;
