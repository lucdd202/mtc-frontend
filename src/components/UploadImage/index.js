import React, { useState, useEffect } from "react";
import { CommonUtils } from "../../utils";
import "./index.scss";
import { Gallery, Item } from "react-photoswipe-gallery";
import "photoswipe/dist/photoswipe.css";
import { convertImage } from "../../utils/function";

const UploadImage = (props) => {
   const [previewImgURL, setPreviewImgURL] = useState("");
   const { onUploadImage, inputImage } = props;

   useEffect(() => {
      if (inputImage) {
         setPreviewImgURL(convertImage(inputImage));
      }
   }, [inputImage]);

   const handleOnChangeImage = async (event) => {
      let data = event.target.files;
      let file = data[0];
      if (file) {
         let base64 = await CommonUtils.getBase64(file);
         let objectUrl = URL.createObjectURL(file);
         setPreviewImgURL(objectUrl);
         onUploadImage(base64);
      }
   };

   const handleClearImg = () => {
      setPreviewImgURL("");
      onUploadImage(null);
   };

   return (
      <>
         <div className="upload-container row">
            <div className="btn-container col-4">
               <label htmlFor="upload-image" className="upload-btn">
                  Đăng ảnh
                  <i className="fas fa-upload"></i>
               </label>
               <input
                  id="upload-image"
                  type="file"
                  hidden
                  onChange={handleOnChangeImage}
               />
               <div className="clear-btn" onClick={handleClearImg}>
                  Xoá ảnh
                  <i className="fas fa-trash"></i>
               </div>
            </div>
            <Gallery>
               <Item
                  original={previewImgURL}
                  thumbnail={previewImgURL}
                  width="1024"
                  height="768"
               >
                  {({ ref, open }) => (
                     <img
                        className="preview-img col-8"
                        ref={ref}
                        onClick={open}
                        src={previewImgURL}
                        alt=""
                     />
                  )}
               </Item>
            </Gallery>
         </div>
      </>
   );
};

export default UploadImage;
