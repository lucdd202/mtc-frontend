import "./index.scss";
import TopRank from "./topRank";
import rank1 from "../../assets/images/medal-1.svg";
import rank2 from "../../assets/images/medal-2.svg";
import rank3 from "../../assets/images/medal-3.svg";
import { Link } from "react-router-dom";
import { getAnalyticDataService } from "../../services/analyticService";
import { useEffect, useState } from "react";
import SectionHeader from "../SectionHeader";

const Ranking = (props) => {
   const { rankKey, rankType } = props;
   const [rank, setRank] = useState([]);

   const handlFetchRankingData = async (key, type) => {
      try {
         const response = await getAnalyticDataService({ key, type });
         if (response && response.status === 200) {
            setRank(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (rankKey && rankType) {
         handlFetchRankingData(rankKey, rankType);
      }
   }, [rankType, rankKey]);

   return (
      <div className="rank-container">
         <SectionHeader
            title={
               rankKey === "VIEW"
                  ? "Đọc nhiều tuần"
                  : rankKey === "COMMENT"
                  ? "Thảo luận tuần"
                  : "Đề cử tuần"
            }
            to={
               rankKey === "VIEW"
                  ? "/bang-xep-hang/WEEK/VIEW"
                  : rankKey === "COMMENT"
                  ? "/bang-xep-hang/WEEK/COMMENT"
                  : "/bang-xep-hang/WEEK/FLOWER"
            }
         />
         <table className="table">
            <tbody>
               {rank.map((item, index) => {
                  switch (index) {
                     case 0:
                        return (
                           <tr key={index}>
                              <th>
                                 <img src={rank1} alt="" />
                              </th>
                              <TopRank rankKey={rankKey} data={item} />
                           </tr>
                        );
                     case 1:
                        return (
                           <tr key={index}>
                              <th>
                                 <img src={rank2} alt="" />
                              </th>
                              <td>
                                 <Link to={`/truyen/${item?.bookId}`}>
                                    {item?.Book?.name}
                                 </Link>
                              </td>
                              <td className="text-center">{item?.count}</td>
                           </tr>
                        );

                     case 2:
                        return (
                           <tr key={index}>
                              <th>
                                 <img src={rank3} alt="" />
                              </th>
                              <td>
                                 <Link to={`/truyen/${item?.bookId}`}>
                                    {item?.Book?.name}
                                 </Link>
                              </td>
                              <td className="text-center">{item?.count}</td>
                           </tr>
                        );

                     default:
                        return (
                           <tr key={index}>
                              <th>{index + 1}</th>
                              <td>
                                 <Link to={`/truyen/${item?.bookId}`}>
                                    {item?.Book?.name}
                                 </Link>
                              </td>
                              <td className="text-center">{item?.count}</td>
                           </tr>
                        );
                  }
               })}
            </tbody>
         </table>
      </div>
   );
};

export default Ranking;
