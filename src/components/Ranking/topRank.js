import { convertImage } from "../../utils/function";
import "./index.scss";
import { Link } from "react-router-dom";
import flower from "../../assets/images/flower.svg";

const TopRank = (props) => {
   const { data, rankKey } = props;

   return (
      <>
         <td className="col-8">
            <Link to={`/truyen/${data?.bookId}`}>{data?.Book?.name}</Link>
            <div>
               <span className="rank-type">{data?.count}</span>
               {rankKey === "VIEW" ? (
                  <i className="fas fa-glasses rank-type-view"></i>
               ) : rankKey === "COMMENT" ? (
                  <i className="far fa-comment-alt rank-type-comment"></i>
               ) : (
                  <img className="rank-type-flower" src={flower} alt="" />
               )}
            </div>
            <div>
               <i className="fas fa-user-edit"></i>
               <span>{data?.BookInfo?.Author?.value}</span>
            </div>
            <div>
               <i className="fas fa-book"></i>
               <span>{data?.BookInfo?.Genre?.value}</span>
            </div>
         </td>
         <td className="col-4">
            <Link
               to={`/truyen/${data?.bookId}`}
               className="b-cover text-center"
            >
               <img
                  className="b-img"
                  alt=""
                  src={convertImage(data?.Book?.image)}
               />
            </Link>
         </td>
      </>
   );
};

export default TopRank;
