import React, { forwardRef, useState } from "react";
import Authentication from "../Modals/Authentication";
import SearchBar from "../SearchBar";
import logo from "../../assets/images/logo.png";
import "./index.scss";
import { UploadOutlined } from "@ant-design/icons";
import { useSelector } from "react-redux";
import {
   selectIsAuthenticated,
   selectUserId,
} from "../../stores/slices/authenSlice";
import { Link } from "react-router-dom";
import GenreDropdown from "../DropdownMenu/Genre";
import ProfileDropdown from "../DropdownMenu/Profile";
import RankDropdown from "../DropdownMenu/Rank";
import NotificationDropDown from "../DropdownMenu/Notification";

const NoAuthenBar = (props) => {
   const { showModal, setType } = props;

   return (
      <>
         <div
            className="login header-item col-4"
            onClick={() => {
               showModal();
               setType(false);
            }}
         >
            Đăng nhập
         </div>
         <div
            className="register header-item col-3"
            onClick={() => {
               showModal();
               setType(true);
            }}
         >
            Đăng ký
         </div>
      </>
   );
};

const AuthenBar = (props) => {
   return (
      <>
         <NotificationDropDown />
         <ProfileDropdown />
      </>
   );
};

const Header = forwardRef((props, ref) => {
   const [isModalOpen, setIsModalOpen] = useState(false);
   const [type, setType] = useState(false); //false là login mà true là register
   const isAuthen = useSelector(selectIsAuthenticated);
   const userId = useSelector(selectUserId);

   const showModal = () => {
      setIsModalOpen(true);
   };
   const handleOk = () => {
      setIsModalOpen(false);
   };
   const handleCancel = () => {
      setIsModalOpen(false);
   };

   return (
      <div className="header-content row" ref={ref}>
         <Link to={"/"} className="col-1">
            <img className="logo" alt="logo" src={logo} />
         </Link>

         <div className="header-left col-3 row">
            <GenreDropdown />
            <RankDropdown />
         </div>
         <div className="header-mid col-4 row">
            <SearchBar className="search-bar" />
         </div>
         <div className="header-right row col-4 container">
            <Link
               to={"/admin/book-manager"}
               className="upload header-item col-5"
            >
               <UploadOutlined className="mr-1" />
               <p>Đăng truyện</p>
            </Link>
            {isAuthen ? (
               <AuthenBar userId={userId} />
            ) : (
               <NoAuthenBar showModal={showModal} setType={setType} />
            )}

            <Authentication
               isModalOpen={isModalOpen}
               handleOk={handleOk}
               handleCancel={handleCancel}
               type={type}
               setType={setType}
            />
         </div>
      </div>
   );
});

export default Header;
