import "./index.scss";

const SectionHeader = (props) => {
   const { title, to } = props;

   return (
      <div className="section-header">
         <h5 className="title mb-3">{title}</h5>
         <a className="more" href={to}>
            Xem chi tiết
         </a>
      </div>
   );
};

export default SectionHeader;
