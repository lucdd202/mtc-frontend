import { useLocation, useNavigate } from "react-router-dom";
import "./index.scss";

const ListGroup = (props) => {
   const { data } = props;
   const location = useLocation();
   const navigate = useNavigate();
   const currentPath = location.pathname;

   const getLastSlash = () => {
      const pathSegments = currentPath.split("/");
      return pathSegments[pathSegments.length - 1];
   };

   const handleButtonClick = (newLastPath) => {
      const lastSlashIndex = currentPath.lastIndexOf("/");
      const newPath = currentPath.slice(0, lastSlashIndex) + `/${newLastPath}`;

      navigate(newPath);
   };

   return (
      <div className="list-group">
         {data.map((item, index) => (
            <div
               className={`category-item ${
                  getLastSlash() === item.path ? "active" : ""
               }`}
               onClick={() => handleButtonClick(item.path)}
               key={index}
            >
               {item.label}
            </div>
         ))}
      </div>
   );
};

export default ListGroup;
