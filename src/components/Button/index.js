import "./index.scss";

const Button = (props) => {
   const { onClick, className, type, label, leftIcon, rightIcon, style } =
      props;

   return (
      <div
         className={`${className} ${type} main-button text-center `}
         style={style}
         onClick={onClick}
      >
         {leftIcon}
         {label}
         {rightIcon}
      </div>
   );
};

export default Button;
