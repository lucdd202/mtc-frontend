import { createSlice } from "@reduxjs/toolkit";

const configSlice = createSlice({
   name: "config",
   initialState: {
      dataConfig: null,
   },
   reducers: {
      setDataConfig: (state, action) => {
         state.dataConfig = action.payload;
      },
   },
});

export const { setDataConfig } = configSlice.actions;
export const selectDataConfig = (state) => state.config.dataConfig;
export default configSlice.reducer;
