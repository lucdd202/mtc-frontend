import { createSlice } from "@reduxjs/toolkit";

const roleSlice = createSlice({
   name: "role",
   initialState: {
      roles: [],
   },
   reducers: {
      fetchAllRoles: (state, action) => {
         state.roles = action.payload;
      },
   },
});

export const { fetchAllRoles } = roleSlice.actions;
export const selectAllRoles = (state) => state.role.roles;
export default roleSlice.reducer;
