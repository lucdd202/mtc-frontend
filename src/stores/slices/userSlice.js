import { createSlice } from "@reduxjs/toolkit";

const userSlice = createSlice({
   name: "user",
   initialState: {
      userDetail: null,
      users: [],
   },
   reducers: {
      fetchAllUsers: (state, action) => {
         state.users = action.payload;
      },
      getUserById: (state, action) => {
         state.userDetail = action.payload;
      },
   },
});

export const { fetchAllUsers, getUserById } = userSlice.actions;
export const selectAllUsers = (state) => state.user.users;
export const selectUserDetail = (state) => state.user.userDetail;
export default userSlice.reducer;
