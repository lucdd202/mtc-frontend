import { createSlice } from "@reduxjs/toolkit";

const initialState = {
   isAuthenticated: false,
   userId: null,
};

const authenSlice = createSlice({
   name: "authen",
   initialState,
   reducers: {
      login: (state, action) => {
         state.isAuthenticated = true;
         state.userId = action.payload;
      },
      logout: (state) => {
         state.isAuthenticated = false;
         state.userId = null;
      },
   },
});

export const { login, logout } = authenSlice.actions;
export const selectIsAuthenticated = (state) => state.authen.isAuthenticated;
export const selectUserId = (state) => state.authen.userId;
export default authenSlice.reducer;
