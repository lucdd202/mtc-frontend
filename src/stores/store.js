import { configureStore } from "@reduxjs/toolkit";
import userReducer from "./slices/userSlice";
import bookReducer from "./slices/bookSlice";
import roleReducer from "./slices/roleSlice";
import authenReducer from "./slices/authenSlice";
import configReducer from "./slices/configSlice";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // sử dụng localStorage

const persistConfig = {
   key: "root",
   storage,
};

const persistedReducer = persistReducer(persistConfig, authenReducer);

const store = configureStore({
   reducer: {
      authen: persistedReducer,
      user: userReducer,
      book: bookReducer,
      role: roleReducer,
      config: configReducer,
   },
   middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
         serializableCheck: false,
      }),
});

export const persistor = persistStore(store);
export default store;
