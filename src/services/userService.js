import api from "../api";

const addNewUserService = (data) => {
   return api.post(`/api/user/add-new-user`, data);
};

const giveFlowerService = (data) => {
   return api.post(`/api/user/give-flower`, data);
};

const getAllUsersService = () => {
   return api.get(`/api/user/get-all-users`);
};

const getUserByIdService = (idInput) => {
   return api.get(`/api/user/get-user-by-id?id=${idInput}`);
};

const updateUserService = (data) => {
   return api.put(`/api/user/update-user`, data);
};

const deleteUserService = (idInput) => {
   return api.delete(`/api/user/delete-user?id=${idInput}`);
};

const changePasswordService = (data) => {
   return api.put(`/api/user/change-password`, data);
};

export {
   addNewUserService,
   getAllUsersService,
   getUserByIdService,
   updateUserService,
   deleteUserService,
   giveFlowerService,
   changePasswordService,
};
