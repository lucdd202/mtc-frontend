import api from "../api";

const loginService = (data) => {
   return api.post(`/api/authen/login`, data);
};

const registerService = (data) => {
   return api.post(`/api/authen/register`, data);
};

export { loginService, registerService };
