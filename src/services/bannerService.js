import api from "../api";

const addNewBannerService = (data) => {
   return api.post(`/api/banner/add-new-banner`, data);
};

const getAllBannersService = () => {
   return api.get(`/api/banner/get-all-banners`);
};

const getRandomBannerService = (number) => {
   return api.get(`/api/banner/get-random-banner?number=${number}`);
};

const getBannerByIdService = (idInput) => {
   return api.get(`/api/banner/get-banner-by-id?id=${idInput}`);
};

const updateBannerService = (data) => {
   return api.put(`/api/banner/update-banner`, data);
};

const deleteBannerService = (idInput) => {
   return api.delete(`/api/banner/delete-banner?id=${idInput}`);
};

export {
   addNewBannerService,
   getAllBannersService,
   getBannerByIdService,
   updateBannerService,
   deleteBannerService,
   getRandomBannerService,
};
