import api from "../api";

const updateConfigurationService = (data) => {
   return api.post(`/api/configuration/update-configuration`, data);
};

const getConfigurationService = (userId) => {
   return api.get(`/api/configuration/get-configuration?userId=${userId}`);
};

export { updateConfigurationService, getConfigurationService };
