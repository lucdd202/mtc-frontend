import api from "../api";

const addNewBookService = (data) => {
   return api.post(`/api/book/add-new-book`, data);
};

const searchBooksService = (data) => {
   return api.post(`/api/book/search-books`, data);
};

const getAllBooksService = () => {
   return api.get(`/api/book/get-all-books`);
};

const getCompletedBooksService = () => {
   return api.get(`/api/book/get-completed-books`);
};

const getHighestRatedBooksService = () => {
   return api.get(`/api/book/get-highest-rated-books`);
};

const getNewestBooksService = () => {
   return api.get(`/api/book/get-newest-books`);
};

const getConvertedBooksService = (idInput) => {
   return api.get(`/api/book/get-converted-books?converterId=${idInput}`);
};

const getBookByIdService = (idInput) => {
   return api.get(`/api/book/get-book-by-id?id=${idInput}`);
};

const updateBookService = (data) => {
   return api.put(`/api/book/update-book`, data);
};

const deleteBookService = (idInput) => {
   return api.delete(`/api/book/delete-book?id=${idInput}`);
};

const getBooksByRankService = (data) => {
   return api.post(`/api/book/get-books-by-rank`, data);
};

export {
   addNewBookService,
   getAllBooksService,
   getBookByIdService,
   updateBookService,
   deleteBookService,
   getConvertedBooksService,
   getNewestBooksService,
   getCompletedBooksService,
   getHighestRatedBooksService,
   searchBooksService,
   getBooksByRankService,
};
