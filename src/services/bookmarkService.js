import api from "../api";

const addRemoveBookmarkService = (data) => {
   return api.post(`/api/bookmark/add-remove-bookmark`, data);
};

const checkIsBookmarkedService = (data) => {
   return api.post(`/api/bookmark/is-bookmarked`, data);
};

const getAllBookmarksService = (userId) => {
   return api.get(`/api/bookmark/get-all-bookmarks?userId=${userId}`);
};

export {
   addRemoveBookmarkService,
   getAllBookmarksService,
   checkIsBookmarkedService,
};
