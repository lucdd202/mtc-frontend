import api from "../api";

const getAnalyticDataService = (data) => {
   return api.get(
      `/api/analytic/get-analytic-data?type=${data.type}&key=${data.key}`
   );
};

export { getAnalyticDataService };
