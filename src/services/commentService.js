import api from "../api";

const addNewCommentService = (data) => {
   return api.post(`/api/comment/add-new-comment`, data);
};

const replyCommentService = (data) => {
   return api.post(`/api/comment/reply-comment`, data);
};

const getAllCommentsService = (bookId) => {
   return api.get(`/api/comment/get-all-comments?bookId=${bookId}`);
};

const getCommentRepliesService = (idInput) => {
   return api.get(`/api/comment/get-all-replies?replyToId=${idInput}`);
};

const deleteCommentService = (idInput) => {
   return api.delete(`/api/comment/delete-comment?id=${idInput}`);
};

export {
   addNewCommentService,
   getAllCommentsService,
   deleteCommentService,
   getCommentRepliesService,
   replyCommentService,
};
