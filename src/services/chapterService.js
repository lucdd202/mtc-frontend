import api from "../api";

const addNewChapterService = (data) => {
   return api.post(`/api/chapter/add-new-chapter`, data);
};

const getAllChaptersService = (bookId) => {
   return api.get(`/api/chapter/get-all-chapters?bookId=${bookId}`);
};

const getChapterByIdService = (idInput) => {
   return api.get(`/api/chapter/get-chapter-by-id?id=${idInput}`);
};

const getChapterByNumberService = (data) => {
   return api.post(`/api/chapter/get-chapter-by-number`, data);
};

const updateChapterService = (data) => {
   return api.put(`/api/chapter/update-chapter`, data);
};

const deleteChapterService = (idInput) => {
   return api.delete(`/api/chapter/delete-chapter?id=${idInput}`);
};

const getLatestChapterService = () => {
   return api.get(`/api/chapter/get-latest-chapters`);
};

const countWeeklyChaptersService = (idInput) => {
   return api.get(`/api/chapter/count-weekly-chapters?bookId=${idInput}`);
};

export {
   addNewChapterService,
   getAllChaptersService,
   getChapterByIdService,
   updateChapterService,
   deleteChapterService,
   getLatestChapterService,
   getChapterByNumberService,
   countWeeklyChaptersService,
};
