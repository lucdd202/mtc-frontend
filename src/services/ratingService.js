import api from "../api";

const createNewRatingService = (data) => {
   return api.post(`/api/rating/create-new-rating`, data);
};

const getAllRatingsService = (bookId) => {
   return api.get(`/api/rating/get-all-ratings?bookId=${bookId}`);
};

const getNewstRatingsService = (limit) => {
   return api.get(`/api/rating/get-newest-ratings?limit=${limit}`);
};

const deleteRatingService = (ratingId) => {
   return api.delete(`/api/rating/delete-rating?id=${ratingId}`);
};

const replyRatingService = (data) => {
   return api.post(`/api/rating/reply-rating`, data);
};

const getRatingRepliesService = (idInput) => {
   return api.get(`/api/rating/get-all-replies?replyToId=${idInput}`);
};

export {
   createNewRatingService,
   getAllRatingsService,
   deleteRatingService,
   replyRatingService,
   getRatingRepliesService,
   getNewstRatingsService,
};
