import api from "../api";

const getAllHistoriesService = (data) => {
   return api.post(`/api/history/get-all-histories`, data);
};

const getHistoryByIdService = (data) => {
   return api.get(
      `/api/history/get-history-by-id?userId=${data.userId}&bookId=${data.bookId}`
   );
};

const deleteHistoryService = (idInput) => {
   return api.delete(`/api/history/delete-history?id=${idInput}`);
};

const switchNotificationService = (idInput) => {
   return api.put(`/api/history/switch-notification?id=${idInput}`);
};

export {
   getAllHistoriesService,
   deleteHistoryService,
   getHistoryByIdService,
   switchNotificationService,
};
