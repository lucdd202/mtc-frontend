import api from "../api";

const getAllNotificationsService = (idInput) => {
   return api.get(`/api/notification/get-all-notifications?userId=${idInput}`);
};
const checkNotificationService = (data) => {
   return api.post(`/api/notification/check-notification`, data);
};
const deleteNotificationService = (idInput) => {
   return api.delete(`/api/notification/delete-notification?id=${idInput}`);
};
const rejectNotificationService = (idInput) => {
   return api.put(`/api/notification/reject-notification?id=${idInput}`);
};

export {
   getAllNotificationsService,
   checkNotificationService,
   deleteNotificationService,
   rejectNotificationService,
};
