import api from "../api";

const addNewCategoryService = (data) => {
   return api.post(`/api/allcode/add-new-code`, data);
};

const getAllCategoriesService = (typeInput) => {
   return api.get(`/api/allcode/get-all-codes?type=${typeInput}`);
};

const convertCategoryService = (keyInput) => {
   return api.get(`/api/allcode/convert-code?key=${keyInput}`);
};

const deleteCategoryService = (idInput) => {
   return api.delete(`/api/allcode/delete-code?id=${idInput}`);
};

export {
   addNewCategoryService,
   getAllCategoriesService,
   deleteCategoryService,
   convertCategoryService,
};
