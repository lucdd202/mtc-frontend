import "./index.scss";
import errImage from "../../assets/images/404-img.png";
import { Link } from "react-router-dom";

const ErrorPage = (props) => {
   return (
      <div className="error-page">
         <img src={errImage} alt="" />
         <h5>Rất tiếc!</h5>
         <p>Nội dung này không tồn tại hoặc bị xoá</p>
         <Link to="/">Trở về trang chủ</Link>
      </div>
   );
};

export default ErrorPage;
