import { BookOutlined, UserOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

export const adminMenu = [
   {
      key: "user",
      label: <Link to={"/admin/user-manager"}>Quản lý người dùng</Link>,
      icon: <UserOutlined />,
   },
   {
      key: "category",
      label: <Link to={"/admin/category-manager"}>Quản lý phân loại</Link>,
      icon: <UserOutlined />,
   },
   {
      key: "book",
      label: <Link to={"/admin/book-manager"}>Quản lý truyện</Link>,
      icon: <BookOutlined />,
   },
   {
      key: "chapter",
      label: <Link to={"/admin/chapter-manager"}>Quản lý chương</Link>,
      icon: <UserOutlined />,
   },
   {
      key: "banner",
      label: <Link to={"/admin/banner-manager"}>Quản lý áp phích</Link>,
      icon: <UserOutlined />,
   },
];
