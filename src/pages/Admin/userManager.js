import { useDispatch, useSelector } from "react-redux";
import {
   fetchAllUsers,
   getUserById,
   selectAllUsers,
   selectUserDetail,
} from "../../stores/slices/userSlice";
import { useEffect, useState } from "react";
import {
   deleteUserService,
   getAllUsersService,
   getUserByIdService,
} from "../../services/userService";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import ModalUser from "../../components/Modals/ModalUser";

const UserManager = () => {
   const users = useSelector(selectAllUsers);
   const dispatch = useDispatch();
   const userDetail = useSelector(selectUserDetail);
   const [type, setType] = useState(false); //false là add mà true là edit

   const [isModalOpen, setIsModalOpen] = useState(false);
   const showModal = () => {
      setIsModalOpen(true);
   };
   const handleOk = () => {
      setIsModalOpen(false);
   };
   const handleCancel = () => {
      setIsModalOpen(false);
   };

   const handleFetchAllUsers = async () => {
      try {
         const response = await getAllUsersService();
         if (response.status === 200) {
            dispatch(fetchAllUsers(response.data));
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteUser = async (idInput) => {
      try {
         const response = await deleteUserService(idInput);
         if (response.status === 200) {
            handleFetchAllUsers();
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllUsers();
   }, []);

   const handleEditUser = async (user) => {
      setType(true);
      try {
         const response = await getUserByIdService(user.id);
         if (response.status === 200) {
            dispatch(getUserById(response.data));
         }
         showModal();
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div>
         <Button type="primary" onClick={showModal}>
            Add new User
         </Button>
         <table className="table table-dark mt-3">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Tên</th>
                  <th>Email</th>
                  <th>Vai trò</th>
                  <th>Thao tác</th>
               </tr>
            </thead>
            <tbody>
               {users &&
                  users.map((item, index) => (
                     <tr key={index}>
                        <th scope="row">{index + 1}</th>
                        <td>{item.name}</td>
                        <td>{item.email}</td>
                        <td>{item.Role.value}</td>
                        <td style={{ display: "flex" }}>
                           <Button
                              type="primary"
                              style={{ backgroundColor: "#faad14" }}
                              icon={<EditOutlined />}
                              onClick={() => handleEditUser(item)}
                           />

                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                              onClick={() => handleDeleteUser(item.id)}
                           />
                        </td>
                     </tr>
                  ))}
            </tbody>
         </table>
         <ModalUser
            isModalOpen={isModalOpen}
            handleCancel={handleCancel}
            handleOk={handleOk}
            userDetail={userDetail}
            type={type}
            handleFetchAllUsers={handleFetchAllUsers}
         />
      </div>
   );
};

export default UserManager;
