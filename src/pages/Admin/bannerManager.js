import { useEffect, useState } from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import ModalBanner from "../../components/Modals/ModalBanner";
import {
   getBannerByIdService,
   getAllBannersService,
   deleteBannerService,
} from "../../services/bannerService";

const BannerManager = () => {
   const [banners, setBanners] = useState([]);
   const [bannerDetail, setBannerDetail] = useState();
   const [type, setType] = useState(false); //false là add mà true là edit

   const [isModalOpen, setIsModalOpen] = useState(false);
   const showModal = () => {
      setIsModalOpen(true);
   };
   const handleOk = () => {
      setIsModalOpen(false);
   };
   const handleCancel = () => {
      setIsModalOpen(false);
   };

   const handleFetchAllBanners = async () => {
      try {
         const response = await getAllBannersService();
         if (response.status === 200) {
            setBanners(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteBanner = async (idInput) => {
      try {
         const response = await deleteBannerService(idInput);
         if (response.status === 200) {
            handleFetchAllBanners();
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllBanners();
   }, []);

   const handleEditBanner = async (banner) => {
      setType(true);
      try {
         const response = await getBannerByIdService(banner.id);
         if (response.status === 200) {
            setBannerDetail(response.data);
         }
         showModal();
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div>
         <Button type="primary" onClick={showModal}>
            Thêm áp phích mới
         </Button>
         <table className="table table-dark mt-3">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Số chương</th>
                  <th>Tiêu đề</th>
                  <th>Thao tác </th>
               </tr>
            </thead>
            <tbody>
               {banners.map((item, index) => {
                  return (
                     <tr key={index}>
                        <th scope="row">{index + 1}</th>
                        <td>{item.bannerNumber}</td>
                        <td>{item.title}</td>
                        <td style={{ display: "flex" }}>
                           <Button
                              type="primary"
                              style={{ backgroundColor: "#faad14" }}
                              icon={<EditOutlined />}
                              onClick={() => handleEditBanner(item)}
                           />
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                              onClick={() => handleDeleteBanner(item.id)}
                           />
                        </td>
                     </tr>
                  );
               })}
            </tbody>
         </table>
         <ModalBanner
            isModalOpen={isModalOpen}
            handleCancel={handleCancel}
            handleOk={handleOk}
            bannerDetail={bannerDetail}
            type={type}
            handleFetchAllBanners={handleFetchAllBanners}
         />
      </div>
   );
};

export default BannerManager;
