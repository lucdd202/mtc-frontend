import { useDispatch } from "react-redux";
import "./index.scss";
import React, { useState } from "react";
import {
   LogoutOutlined,
   MenuFoldOutlined,
   MenuUnfoldOutlined,
} from "@ant-design/icons";
import { Button, Layout, Menu, theme } from "antd";
import { adminMenu } from "./adminMenu";
import { Link, Navigate, Route, Routes } from "react-router-dom";
import UserManager from "./userManager";
import BookManager from "./bookManager";
import CategoryManager from "./categoryManager";
import { logout } from "../../stores/slices/authenSlice";
import logo from "../../assets/images/logo.png";
import ChapterManager from "./chapterManager";
import BannerManager from "./bannerManager";

const { Header, Content, Footer, Sider } = Layout;

const AdminPage = () => {
   const dispatch = useDispatch();

   const handleLogout = async () => {
      try {
         dispatch(logout());
      } catch (e) {}
   };
   const [collapsed, setCollapsed] = useState(false);
   const {
      token: { colorBgContainer },
   } = theme.useToken();

   return (
      <Layout
         style={{
            minHeight: "100vh",
         }}
      >
         <Sider collapsible collapsed={collapsed} trigger={null}>
            <Link
               to="/"
               className="demo-logo-vertical"
               style={{
                  background: `url(${logo}) center center / contain no-repeat`,
                  cursor: "pointer",
                  height: "70px",
                  width: "100%",
                  display: "flex",
               }}
            ></Link>
            {/* <Link to="/" ></Link> */}
            <Menu
               theme="dark"
               defaultSelectedKeys={adminMenu[0].key}
               mode="inline"
               items={adminMenu}
            />
         </Sider>
         <Layout>
            <Header
               style={{
                  display: "flex",
                  justifyContent: "space-between",
                  padding: 0,
                  background: colorBgContainer,
               }}
            >
               <Button
                  type="text"
                  icon={
                     collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />
                  }
                  onClick={() => setCollapsed(!collapsed)}
                  style={{
                     fontSize: "16px",
                     width: 64,
                     height: 64,
                  }}
               />
               <Button
                  type="text"
                  icon={<LogoutOutlined />}
                  onClick={handleLogout}
                  style={{
                     fontSize: "16px",
                     width: 64,
                     height: 64,
                  }}
               />
            </Header>
            <Content
               style={{
                  margin: "0 16px",
               }}
            >
               <div
                  style={{
                     padding: 24,
                     minHeight: 360,
                     background: colorBgContainer,
                  }}
               >
                  <Routes path="/admin" element={<AdminPage />}>
                     <Route path="/user-manager" element={<UserManager />} />
                     <Route path="/book-manager" element={<BookManager />} />
                     <Route
                        path="/category-manager"
                        element={<CategoryManager />}
                     />{" "}
                     <Route
                        path="/chapter-manager"
                        element={<ChapterManager />}
                     />
                     <Route
                        path="/banner-manager"
                        element={<BannerManager />}
                     />
                     <Route
                        path="/*"
                        element={<Navigate to="/admin/user-manager" replace />}
                     />
                  </Routes>
               </div>
            </Content>
            <Footer
               style={{
                  textAlign: "center",
               }}
            >
               Metruyenchu ©2023
            </Footer>
         </Layout>
      </Layout>
   );
};
export default AdminPage;
