import { useEffect, useState } from "react";
import {
   deleteBookService,
   getAllBooksService,
   getBookByIdService,
} from "../../services/bookService";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button } from "antd";
import ModalBook from "../../components/Modals/ModalBook";

const BookManager = () => {
   const [books, setBooks] = useState([]);
   const [bookDetail, setBookDetail] = useState();
   const [type, setType] = useState(false); //false là add mà true là edit

   const [isModalOpen, setIsModalOpen] = useState(false);
   const showModal = () => {
      setIsModalOpen(true);
   };
   const handleOk = () => {
      setIsModalOpen(false);
   };
   const handleCancel = () => {
      setIsModalOpen(false);
   };

   const handleFetchAllBooks = async () => {
      try {
         const response = await getAllBooksService();
         if (response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteBook = async (idInput) => {
      try {
         const response = await deleteBookService(idInput);
         if (response.status === 200) {
            handleFetchAllBooks();
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllBooks();
   }, []);

   const handleEditBook = async (book) => {
      setType(true);
      try {
         const response = await getBookByIdService(book.id);
         if (response.status === 200) {
            setBookDetail(response.data);
         }
         showModal();
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div>
         <Button
            type="primary"
            onClick={() => {
               showModal();
               setType(false);
            }}
         >
            Add new Book
         </Button>
         <table className="table table-dark mt-3">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Truyện</th>
                  <th>Thể loại</th>
                  <th>Tác giả</th>
                  <th>Action</th>
               </tr>
            </thead>
            <tbody>
               {books &&
                  books.map((item, index) => {
                     return (
                        <tr key={index}>
                           <th scope="row">{index + 1}</th>
                           <td>{item.name}</td>
                           <td>{item.BookInfo?.genreId}</td>
                           <td>{item.BookInfo?.authorId}</td>
                           <td style={{ display: "flex" }}>
                              <Button
                                 type="primary"
                                 style={{ backgroundColor: "#faad14" }}
                                 icon={<EditOutlined />}
                                 onClick={() => handleEditBook(item)}
                              />

                              <Button
                                 icon={<DeleteOutlined />}
                                 type="primary"
                                 danger
                                 onClick={() => handleDeleteBook(item.id)}
                              />
                           </td>
                        </tr>
                     );
                  })}
            </tbody>
         </table>
         <ModalBook
            isModalOpen={isModalOpen}
            handleCancel={handleCancel}
            handleOk={handleOk}
            bookDetail={bookDetail}
            type={type}
            handleFetchAllBooks={handleFetchAllBooks}
         />
      </div>
   );
};

export default BookManager;
