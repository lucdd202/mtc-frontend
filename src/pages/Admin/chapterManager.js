import { useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Select } from "antd";
import { getConvertedBooksService } from "../../services/bookService";
import { selectUserId } from "../../stores/slices/authenSlice";
import ModalChapter from "../../components/Modals/ModalChapter";
import {
   getChapterByIdService,
   getAllChaptersService,
   deleteChapterService,
} from "../../services/chapterService";

const ChapterManager = () => {
   const userId = useSelector(selectUserId);

   const [convertedBooks, setConvertedBooks] = useState([]);
   const chaptersOption = convertedBooks.map((item) => ({
      value: item.id,
      label: item.name,
   }));
   const [bookId, setBookId] = useState(chaptersOption[0]?.value);
   const [chapters, setChapters] = useState([]);
   const [chapterDetail, setChapterDetail] = useState();

   const handleFetchConvertedBooks = async (converterId) => {
      try {
         const response = await getConvertedBooksService(converterId);
         if (response.status === 200) {
            setConvertedBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (userId) {
         handleFetchConvertedBooks(userId);
      }
   }, [userId]);

   const [type, setType] = useState(false); //false là add mà true là edit

   const [isModalOpen, setIsModalOpen] = useState(false);
   const showModal = () => {
      setIsModalOpen(true);
   };
   const handleOk = () => {
      setIsModalOpen(false);
   };
   const handleCancel = () => {
      setIsModalOpen(false);
   };

   const handleFetchAllChapters = async (idInput) => {
      try {
         const response = await getAllChaptersService(idInput);
         if (response.status === 200) {
            setChapters(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteChapter = async (idInput) => {
      try {
         const response = await deleteChapterService(idInput);
         if (response.status === 200) {
            handleFetchAllChapters(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (bookId) {
         handleFetchAllChapters(bookId);
      }
   }, [bookId]);

   const handleEditChapter = async (chapter) => {
      setType(true);
      try {
         const response = await getChapterByIdService(chapter.id);
         if (response.status === 200) {
            setChapterDetail(response.data);
         }
         showModal();
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div>
         <Select
            style={{ width: "200px" }}
            defaultValue={chaptersOption[0]?.value}
            options={chaptersOption}
            onChange={(value) => setBookId(+value)}
         />

         <Button type="primary" onClick={showModal}>
            Thêm chương mới
         </Button>
         <table className="table table-dark mt-3">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Số chương</th>
                  <th>Tiêu đề</th>
                  <th>Thao tác </th>
               </tr>
            </thead>
            <tbody>
               {chapters.map((item, index) => {
                  return (
                     <tr key={index}>
                        <th scope="row">{index + 1}</th>
                        <td>{item.chapterNumber}</td>
                        <td>{item.title}</td>
                        <td style={{ display: "flex" }}>
                           <Button
                              type="primary"
                              style={{ backgroundColor: "#faad14" }}
                              icon={<EditOutlined />}
                              onClick={() => handleEditChapter(item)}
                           />
                           <Button
                              icon={<DeleteOutlined />}
                              type="primary"
                              danger
                              onClick={() => handleDeleteChapter(item.id)}
                           />
                        </td>
                     </tr>
                  );
               })}
            </tbody>
         </table>
         <ModalChapter
            isModalOpen={isModalOpen}
            handleCancel={handleCancel}
            handleOk={handleOk}
            chapterDetail={chapterDetail}
            bookId={+bookId}
            type={type}
            handleFetchAllChapters={handleFetchAllChapters}
         />
      </div>
   );
};

export default ChapterManager;
