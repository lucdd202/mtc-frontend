import { useEffect, useState } from "react";
import { DeleteOutlined } from "@ant-design/icons";
import { Button, Select } from "antd";
import {
   addNewCategoryService,
   deleteCategoryService,
   getAllCategoriesService,
} from "../../services/categoryService";
import { categoryMenu } from "../../utils/constant";

const CategoryManager = () => {
   const [categoryData, setCategoryData] = useState([]);
   const [form, setForm] = useState({
      type: categoryMenu[0].value,
      value: "",
   });

   const handleFetchAllCategories = async (typeInput) => {
      try {
         const response = await getAllCategoriesService(typeInput);
         if (response.status === 200) {
            setCategoryData(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleAddNewCode = async () => {
      try {
         const response = await addNewCategoryService(form);
         if (response) {
            handleFetchAllCategories(form.type);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteCategory = async (idInput) => {
      try {
         const response = await deleteCategoryService(idInput);
         if (response.status === 200) {
            handleFetchAllCategories(form.type);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllCategories(form.type);
   }, [form]);

   const handleOnChange = (e) => {
      const { name, value } = e.target;
      setForm((prev) => ({
         ...prev,
         [name]: value,
      }));
   };

   return (
      <div>
         <Select
            style={{ width: "200px" }}
            defaultValue={categoryMenu[0].label}
            options={categoryMenu}
            onChange={(value) => setForm((prev) => ({ ...prev, type: value }))}
         />
         Nội dung
         <input name="value" value={form.value} onChange={handleOnChange} />
         <Button type="primary" onClick={handleAddNewCode}>
            Add new code
         </Button>
         <table className="table table-dark mt-3">
            <thead>
               <tr>
                  <th>ID</th>
                  <th>Nội dung</th>
                  <th>Loại</th>
                  <th>Mã loại</th>
                  <th>Thao tác </th>
               </tr>
            </thead>
            <tbody>
               {categoryData &&
                  categoryData.map((item, index) => {
                     return (
                        <tr key={index}>
                           <th scope="row">{index + 1}</th>
                           <td>{item.value}</td>
                           <td>{item.type}</td>
                           <td>{item.key}</td>
                           <td style={{ display: "flex" }}>
                              <Button
                                 icon={<DeleteOutlined />}
                                 type="primary"
                                 danger
                                 onClick={() => handleDeleteCategory(item.id)}
                              />
                           </td>
                        </tr>
                     );
                  })}
            </tbody>
         </table>
      </div>
   );
};

export default CategoryManager;
