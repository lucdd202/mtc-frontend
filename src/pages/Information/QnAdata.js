export const QnA = [
   {
      label: "Làm sao đổi màu nền,cỡ chữ,font chữ",
      value: "- Dùng web trên máy tính: ấn vào nút bánh răng ở bên trái khung đọc truyện. Video hướng dẫn: https://static.cdnno.com/videos/howto/setting-desktop.mp4 \n- Dùng web trên điện thoại: ấn vào giữa nội dung chương truyện rồi ấn nút bánh răng ở bên dưới. Video hướng dẫn: https://static.cdnno.com/videos/howto/setting-mobile.mp4 \n - Dùng app Mê Truyện Chữ - Nữ Hiệp trên điện thoại: ấn vào giữa nội dung chương truyện rồi chọn Cài đặt. Vdeo hướng dẫn: https://static.cdnno.com/videos/howto/setting-app.mp4 \n\n Tips: ở trên app ngoài cài đặt màu nền, cỡ chữ, font chữ bạn còn có thể cài đặt chế độ đọc truyện theo Lật trang hoặc Cuộn dọc, ở chế độ Lật trang bạn có thể ấn vào mép phải màn hình để lật trang thay vì vuốt",
   },
   {
      label: "Hoa là gì và dùng để làm gì",
      value: "Hoa là một loại phiếu đề cử, dùng để đề cử truyện bạn thích, giúp cho nhiều người biết đến truyện đó hơn và hỗ trợ người đăng truyện. Truyện càng được đề cử nhiều Hoa thì đang có thứ hạng tốt trên Bảng Xếp Hạng",
   },
   {
      label: "Làm sao để có hoa",
      value: "Hoa được cộng tự động vào tài khoản của bạn vào lúc 0h00 hàng ngày, số lượng nhận được tùy vào Đẳng Cấp của bạn.",
   },
];
