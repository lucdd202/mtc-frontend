import { useLocation } from "react-router";
import { QnA } from "./QnAdata";
import "./index.scss";

const InformationPage = (props) => {
   const location = useLocation();
   const questionId = location.hash;

   return (
      <div className="information-page">
         <h5 className="infomation-title">Những câu hỏi thường gặp</h5>
         {QnA.map((item, index) => (
            <div className="information-container" key={index}>
               <div
                  href={`#${index}`}
                  className="information-question"
                  data-toggle="collapse"
               >
                  {item.label}
               </div>

               <div
                  id={index}
                  className={`${
                     questionId === `#${index}` ? "show" : ""
                  } collapse `}
               >
                  <div className=" information-answer">{item.value}</div>
               </div>
            </div>
         ))}
      </div>
   );
};

export default InformationPage;
