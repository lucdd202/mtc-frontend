import "./index.scss";
import { useEffect } from "react";
import RankSection from "./Section/RankSection";
import HelpSection from "./Section/HelpSection";
import BookSection from "./Section/BookSection";
import HistorySection from "./Section/HistorySection";
import RatingSection from "./Section/RatingSection";
import NewBookSection from "./Section/NewBookSection";
import LatestSection from "./Section/LatestSection";

const HomePage = (props) => {
   useEffect(() => {
      document.title = "Mê Truyện Chữ";
   }, []);

   return (
      <div className="home-page">
         <div className="row">
            <div className="col-md-9 col-sm-12">
               <BookSection type="recommend" title="Biên tập viên đề cử" />
            </div>
            <div className="col-md-3 col-sm-12">
               <div>
                  <HistorySection />
               </div>
               <hr />
               <div>
                  <HelpSection />
               </div>
            </div>
         </div>

         <div className="latest-section mt-5">
            <LatestSection />
         </div>

         <div className="mt-3">
            <RankSection />
         </div>

         <div className="row mt-5">
            <div className="col-8">
               <BookSection title="Đánh giá cao" type="rated" />
            </div>
            <div className="col-4">
               <RatingSection />
            </div>
         </div>

         <div className="row my-5 new-finish-row">
            <div className="col-4 left-item">
               <NewBookSection type="newest" />
            </div>
            <div className="col-8 right-item">
               <BookSection title="Mới hoàn thành" type="completed" />
            </div>
         </div>
      </div>
   );
};

export default HomePage;
