import "./index.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import { EffectCoverflow, Navigation } from "swiper/modules";
import { getNewestBooksService } from "../../../../services/bookService";
import { useEffect, useState } from "react";
import { convertImage } from "../../../../utils/function";
import BookCard from "../../../../components/Card/BookCard";
import { Link } from "react-router-dom";
import Loading from "../../../../components/Loading";
import SectionHeader from "../../../../components/SectionHeader";

const NewBookSection = (props) => {
   const { type } = props;
   const [books, setBooks] = useState([]);
   const [activeSlideIndex, setActiveSlideIndex] = useState(1);
   const selectedBook = books[activeSlideIndex];

   const handleFetchNewestBooks = async () => {
      try {
         const response = await getNewestBooksService();
         if (response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchNewestBooks();
   }, []);

   return (
      <div className="new-book-section">
         {type === "newest" && <SectionHeader title="Mới đăng" to="/truyen" />}
         {books.length > 0 ? (
            <div className={`slider-thumb ${type}`}>
               <Swiper
                  effect={"coverflow"}
                  grabCursor={true}
                  centeredSlides
                  slidesPerView={type === "newest" ? 2.3 : 1}
                  initialSlide={1}
                  loop={true}
                  navigation={true}
                  modules={[EffectCoverflow, Navigation]}
                  className="mySwiper"
                  onSlideChange={(swiper) =>
                     setActiveSlideIndex(swiper.realIndex)
                  }
               >
                  {books.map((item, index) => (
                     <SwiperSlide key={index}>
                        <Link
                           to={`/truyen/${item?.id}`}
                           className={`slider-img ${type}`}
                        >
                           <img
                              src={convertImage(item?.image)}
                              style={{
                                 width: "100%",
                                 height: "200px",
                              }}
                              alt=""
                           />
                        </Link>
                     </SwiperSlide>
                  ))}
               </Swiper>
               <div className="book-info">
                  <BookCard data={selectedBook} image={false} />
                  {type === "newest" && (
                     <Link
                        to={`/truyen/${selectedBook?.id}`}
                        className="btn-read"
                     >
                        Đọc ngay
                     </Link>
                  )}
               </div>
            </div>
         ) : (
            <Loading />
         )}
      </div>
   );
};

export default NewBookSection;
