import "./index.scss";
import { useSelector } from "react-redux";
import { selectUserId } from "../../../../stores/slices/authenSlice";
import {
   deleteHistoryService,
   getAllHistoriesService,
} from "../../../../services/historyService";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { convertImage } from "../../../../utils/function";
import Loading from "./../../../../components/Loading/index";
import SectionHeader from "../../../../components/SectionHeader";

const HistorySection = (props) => {
   const userId = useSelector(selectUserId);
   const [histories, setHistories] = useState([]);

   const handleFetchAllHistories = async (userId, limit) => {
      try {
         const response = await getAllHistoriesService({ userId, limit });
         if (response.status === 200) {
            setHistories(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (userId) {
         handleFetchAllHistories(userId, 5);
      }
   }, [userId]);

   const handleDeleteHistory = async (idInput) => {
      try {
         const response = await deleteHistoryService(idInput);
         if (response.status === 200) {
            handleFetchAllHistories(userId, 5);
         }
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div className="history-section">
         <SectionHeader title="Đang đọc" to="/tai-khoan/tu-truyen" />
         <div className={` history-container row`}>
            {histories.length > 0 ? (
               histories.map((item, index) => (
                  <div className="history-item col-12 row" key={index}>
                     <Link
                        to={`truyen/${item.bookId}`}
                        className="col-2 history-image"
                        style={{
                           backgroundImage: `url(${convertImage(
                              item?.Book?.image
                           )})`,
                        }}
                     >
                        {" "}
                     </Link>
                     <div className="col-8 history-info">
                        <Link
                           to={`truyen/${item.bookId}`}
                           className="history-book"
                        >
                           {item?.Book?.name}
                        </Link>
                        <div className="history-chapter">
                           Đã đọc: {item.readAt}/{item?.Book?.Chapters?.length}
                           <i
                              className="fas fa-trash delete-history mx-2"
                              onClick={() => handleDeleteHistory(item.id)}
                           ></i>
                        </div>
                     </div>
                     <Link
                        to={`/truyen/${item.bookId}/${item.readAt}`}
                        className="history-read col-1"
                     >
                        Đọc tiếp
                     </Link>
                  </div>
               ))
            ) : (
               <Loading />
            )}
         </div>
      </div>
   );
};

export default HistorySection;
