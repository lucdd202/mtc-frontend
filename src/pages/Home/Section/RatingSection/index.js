import { useEffect, useState } from "react";
import RatingCard from "../../../../components/Card/RatingCard";
import { getNewstRatingsService } from "../../../../services/ratingService";
import "./index.scss";
import SectionHeader from "../../../../components/SectionHeader";

const RatingSection = (props) => {
   const [ratings, setRatings] = useState([]);

   const handleGetNewestRatings = async (limit) => {
      try {
         const response = await getNewstRatingsService(limit);
         if (response.status === 200) {
            setRatings(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleGetNewestRatings(3);
   }, []);

   return (
      <div className="rating-section">
         <SectionHeader title="Mới đánh giá" to="/truyen" />
         <div className="rating-container">
            {ratings.map((item, index) => (
               <RatingCard data={item} key={index} />
            ))}
         </div>
      </div>
   );
};

export default RatingSection;
