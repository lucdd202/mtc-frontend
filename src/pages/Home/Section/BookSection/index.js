import "./index.scss";
import {
   getAllBooksService,
   getCompletedBooksService,
   getHighestRatedBooksService,
} from "../../../../services/bookService";
import { Fragment, useCallback, useEffect, useState } from "react";
import BookCard from "../../../../components/Card/BookCard";
import Loading from "./../../../../components/Loading/index";
import SectionHeader from "./../../../../components/SectionHeader/index";

const BookSection = (props) => {
   const { title, type, data } = props;
   const [books, setBooks] = useState([]);

   const handleFetchAllBooks = useCallback(async () => {
      try {
         let response;
         if (type === "recommend") {
            response = await getAllBooksService();
         }
         if (type === "completed") {
            response = await getCompletedBooksService();
         }
         if (type === "rated") {
            response = await getHighestRatedBooksService();
         }

         if (response && response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [type]);

   useEffect(() => {
      handleFetchAllBooks();
   }, [handleFetchAllBooks]);

   const showBook = (arr) => {
      const result = [];
      for (let i = 0; i < arr.length; i += 2) {
         result.push(
            <Fragment key={i}>
               <div className="row book-row">
                  <BookCard data={arr[i]} type={type} image={true} />
                  {i + 1 < arr.length && (
                     <BookCard data={arr[i + 1]} type={type} image={true} />
                  )}
               </div>
               {i + 1 < arr.length && arr[i + 2] && <hr />}
            </Fragment>
         );
      }
      return result;
   };

   return (
      <div className="book-section">
         <SectionHeader title={title} to="truyen" />
         {books?.length > 0 || data?.length > 0 ? (
            <div className="book-container">{showBook(data ?? books)}</div>
         ) : (
            <Loading />
         )}
      </div>
   );
};

export default BookSection;
