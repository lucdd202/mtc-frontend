import { getLatestChapterService } from "../../../../services/chapterService";
import "./index.scss";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { convertTime } from "../../../../utils/function";
import SectionHeader from "../../../../components/SectionHeader";

const LatestSection = (props) => {
   const [latestChapters, setLatestChapters] = useState([]);

   const handleGetLastestChapters = async () => {
      try {
         const response = await getLatestChapterService();
         if (response.status === 200) {
            setLatestChapters(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleGetLastestChapters();
   }, []);

   return (
      <div className="latest-section mt-2">
         <SectionHeader title="Mới cập nhật" to="/truyen" />
         <table className=" table-latest mt-3">
            <tbody>
               {latestChapters.map((item, index) => {
                  return (
                     <tr key={index}>
                        <td>{item?.Book?.BookInfo?.Genre?.value}</td>
                        <td className="book">
                           <Link to={`/truyen/${item.bookId}`}>
                              {item?.Book?.name}
                           </Link>
                        </td>
                        <td className="chapter">
                           <Link
                              to={`/truyen/${item.bookId}/${item.chapterNumber}`}
                           >
                              Chương {item.chapterNumber}: {item.title}
                           </Link>
                        </td>
                        <td className="author">
                           {item?.Book?.BookInfo?.Author?.value}
                        </td>
                        <td>{item?.Book?.Converter?.name}</td>
                        <td>{convertTime(item?.createdAt)}</td>
                     </tr>
                  );
               })}
            </tbody>
         </table>
      </div>
   );
};

export default LatestSection;
