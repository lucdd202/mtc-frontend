import Ranking from "../../../../components/Ranking";

const RankSection = (props) => {
   return (
      <div className="rank-section row">
         <div className="col-lg-4 col-md-6 col-sm-12 mt-md-3">
            <Ranking rankKey="VIEW" rankType="WEEK" />
         </div>
         <div className="col-lg-4 col-md-6 col-sm-12 mt-md-3">
            <Ranking rankKey="COMMENT" rankType="WEEK" />
         </div>
         <div className="col-lg-4 col-md-6 col-sm-12 mt-md-3">
            <Ranking rankKey="FLOWER" rankType="WEEK" />
         </div>
      </div>
   );
};

export default RankSection;
