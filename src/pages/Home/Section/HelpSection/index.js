import { Link } from "react-router-dom";
import { QnA } from "../../../Information/QnAdata";
import "./index.scss";
import SectionHeader from "./../../../../components/SectionHeader/index";

const HelpSection = (props) => {
   return (
      <div className="help-section">
         <SectionHeader title="Hướng dẫn" to="/thong-tin" />
         <ul className="help-list">
            {QnA.map((item, index) => (
               <li key={index} className="help-item">
                  <Link to={`/thong-tin/#${index}`}>{item.label}</Link>
               </li>
            ))}
         </ul>
      </div>
   );
};

export default HelpSection;
