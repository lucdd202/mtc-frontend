import { Tabs } from "antd";
import "./index.scss";
import { useSelector } from "react-redux";
import { selectUserId } from "../../../stores/slices/authenSlice";
import { useCallback, useEffect, useState } from "react";
import {
   deleteHistoryService,
   getAllHistoriesService,
   switchNotificationService,
} from "../../../services/historyService";
import { getAllBookmarksService } from "../../../services/bookmarkService";
import Loading from "../../../components/Loading";
import { convertImage, convertTime } from "../../../utils/function";
import { Link, useNavigate } from "react-router-dom";

const TabData = (props) => {
   const { type } = props;
   const userId = useSelector(selectUserId);
   const [data, setData] = useState([]);
   const navigate = useNavigate();

   const handleFetchData = async (userId, type) => {
      try {
         let response;
         if (type === "history") {
            response = await getAllHistoriesService({ userId });
         } else if (type === "bookmark") {
            response = await getAllBookmarksService(userId);
         }
         if (response && response.status === 200) {
            setData(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (userId) {
         handleFetchData(userId, type);
      }
   }, [userId, type]);

   const handleDeleteHistory = async (idInput) => {
      try {
         const response = await deleteHistoryService(idInput);
         if (response.status === 200) {
            handleFetchData(userId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleSwitchNotification = useCallback(
      async (idInput) => {
         try {
            const response = await switchNotificationService(idInput);
            if (response && response.status === 200) {
               handleFetchData(userId, "history");
            }
         } catch (e) {
            console.log(e);
         }
      },
      [userId]
   );

   return (
      <div className="tab-data">
         {data.length > 0 ? (
            data.map((item, index) => (
               <div className="data-item row my-3" key={index}>
                  <img
                     className="col-1"
                     src={convertImage(item.Book.image)}
                     alt=""
                     onClick={() => navigate(`/truyen/${item.bookId}`)}
                  />
                  <div className="col-10">
                     <Link className="book-title" to={`/truyen/${item.bookId}`}>
                        {item.Book.name}
                     </Link>
                     {type === "history" && (
                        <p style={{ color: "#666" }}>
                           Đã đọc: {item.readAt}/{item.Book.Chapters.length}{" "}
                        </p>
                     )}
                     {type === "bookmark" && (
                        <div style={{ color: "#666" }}>
                           <i className="far fa-clock mr-2"></i>
                           {convertTime(item.createdAt)}
                        </div>
                     )}
                  </div>
                  <div className="col-1">
                     {type === "history" && (
                        <i
                           className={`fas fa-bell ${
                              item.isNotification ? "active" : ""
                           }`}
                           onClick={() => handleSwitchNotification(item.id)}
                        ></i>
                     )}
                     <i
                        className="fas fa-trash delete-history mx-2"
                        onClick={() => handleDeleteHistory(item.id)}
                     ></i>
                  </div>
               </div>
            ))
         ) : (
            <Loading />
         )}
      </div>
   );
};

const bookcaseTabs = [
   {
      key: "1",
      label: "Đang đọc",
      children: <TabData type="history" />,
   },
   {
      key: "2",
      label: "Đánh dấu",
      children: <TabData type="bookmark" />,
   },
];

const Bookcase = (props) => {
   return (
      <div className="bookcase">
         <Tabs
            className="bookcase-tabs"
            defaultActiveKey="1"
            items={bookcaseTabs}
         />
      </div>
   );
};

export default Bookcase;
