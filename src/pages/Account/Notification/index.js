import { useSelector } from "react-redux";
import "./index.scss";
import { selectUserId } from "../../../stores/slices/authenSlice";
import { Fragment, useCallback, useEffect, useState } from "react";
import {
   checkNotificationService,
   deleteNotificationService,
   getAllNotificationsService,
   rejectNotificationService,
} from "../../../services/notificationService";
import { useNavigate } from "react-router-dom";
import { convertImage, convertTime } from "../../../utils/function";
import Loading from "../../../components/Loading";

const NotificationPage = (props) => {
   const userId = useSelector(selectUserId);
   const [notifications, setNotifications] = useState([]);
   const navigate = useNavigate();

   const handleFetchAllNotification = useCallback(async () => {
      try {
         const response = await getAllNotificationsService(userId);
         if (response && response.status === 200) {
            setNotifications(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId]);

   const handleCheckNotification = async (id, userId) => {
      try {
         const response = await checkNotificationService({ id, userId });
         if (response && response.status === 200) {
            handleFetchAllNotification();
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleRejectNotification = async (idInput) => {
      try {
         const response = await rejectNotificationService(idInput);
         if (response && response.status === 200) {
            handleFetchAllNotification();
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleDeleteNotification = async (idInput) => {
      try {
         const response = await deleteNotificationService(idInput);
         if (response && response.status === 200) {
            handleFetchAllNotification();
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllNotification();
   }, [handleFetchAllNotification]);

   return (
      <div className="notification-page">
         <div className="notification-header">
            <div>Thông báo</div>
            <i
               className="fas fa-check"
               onClick={() => handleCheckNotification(null, userId)}
            ></i>
         </div>
         <hr />
         <div className="notification-container">
            {notifications.length > 0 ? (
               notifications.map((notification, index) => (
                  <Fragment key={index}>
                     <div className="notification-item flex">
                        <img
                           src={convertImage(notification.User.avatar)}
                           alt=""
                           onClick={() =>
                              navigate(`/ho-so/${notification.fromId}`)
                           }
                        />
                        <div className="notification-content">
                           <div
                              className="notification-title"
                              style={{
                                 fontWeight: notification.isRead && "400",
                              }}
                              onClick={() => {
                                 handleCheckNotification(notification.id);
                                 navigate(`/truyen/${notification.bookId}`);
                              }}
                           >
                              {notification.content}
                           </div>
                           <div className="notification-time">
                              <i className="fas fa-clock mr-2"></i>
                              {convertTime(notification.createdAt)}
                           </div>
                        </div>
                        <div className="dropleft">
                           <div
                              type="button"
                              className="dropdown-toggle"
                              data-toggle="dropdown"
                              aria-haspopup="true"
                              aria-expanded="false"
                           ></div>
                           <div className="dropdown-menu">
                              <div
                                 className="dropdown-item"
                                 onClick={() =>
                                    handleCheckNotification(notification.id)
                                 }
                              >
                                 Đánh dấu đã đọc
                              </div>
                              <div
                                 className="dropdown-item"
                                 onClick={() =>
                                    handleRejectNotification(notification.id)
                                 }
                              >
                                 Ngừng nhận thông báo này
                              </div>
                              <div
                                 className="dropdown-item"
                                 onClick={() =>
                                    handleDeleteNotification(notification.id)
                                 }
                              >
                                 Xóa thông báo
                              </div>
                           </div>
                        </div>
                     </div>
                     {notifications[index + 1] && <hr />}
                  </Fragment>
               ))
            ) : (
               <Loading />
            )}
         </div>
      </div>
   );
};

export default NotificationPage;
