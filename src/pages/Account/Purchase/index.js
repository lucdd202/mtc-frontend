import "./index.scss";
import viettel from "../../../assets/images/viettel-icon.png";
import mobifone from "../../../assets/images/mobifone-icon.png";
import vinaphone from "../../../assets/images/vinaphone-icon.png";
import masterCard from "../../../assets/images/master-card.png";
import visa from "../../../assets/images/visa.png";
import paypal from "../../../assets/images/paypal.png";

const Purchase = (props) => {
   return (
      <div className="purchase">
         <h5>Mua kẹo</h5>
         <div>
            <b>Vui lòng đọc kỹ nội dung bên dưới trước khi mua</b>
            <ul>
               <li>
                  Một khi đã mua Kẹo, sẽ không được hoàn lại vì bất cứ lý do nào
               </li>
               <li>
                  Kẹo chỉ được cộng cho bạn khi nào chúng tôi chắc chắn rằng đã
                  nhận được thanh toán của bạn
               </li>
               <li>
                  Bạn có thể mua Kẹo thông qua một trong các hình thức thanh
                  toán bên dưới
               </li>
            </ul>
         </div>
         <a href="/#" className="payment-item">
            <div class="mb-2">
               <img src={paypal} class="mx-1" alt="" />
               <img src={visa} class="mx-1" alt="" />
               <img src={masterCard} class="mx-1" alt="" />
            </div>
            <span>Thanh toán thông qua Paypal, Visa, Master Card</span>
         </a>
         <a href="/#" className="payment-item">
            <div class="mb-2">
               <img src={vinaphone} class="mx-1" alt="" />
               <img src={mobifone} class="mx-1" alt="" />
               <img src={viettel} class="mx-1" alt="" />
            </div>
            <span>Thanh toán thông qua thẻ điện thoại, thẻ game</span>
         </a>
      </div>
   );
};

export default Purchase;
