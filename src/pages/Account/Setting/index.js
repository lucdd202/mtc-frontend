import { Tabs } from "antd";
import "./index.scss";
import ProfileTab from "./Tabs/ProfileTab";
import SecurityTab from "./Tabs/SecurityTab";

const settingTabs = [
   {
      key: "1",
      label: "Hồ sơ",
      children: <ProfileTab />,
   },
   {
      key: "2",
      label: "Bảo mật",
      children: <SecurityTab />,
   },
];

const Setting = (props) => {
   return (
      <div className="setting">
         <Tabs
            className="setting-tabs"
            defaultActiveKey="1"
            items={settingTabs}
         />
      </div>
   );
};

export default Setting;
