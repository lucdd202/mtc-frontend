import { useSelector } from "react-redux";
import Button from "../../../../../components/Button";
import "./index.scss";
import { selectUserId } from "../../../../../stores/slices/authenSlice";
import { changePasswordService } from "../../../../../services/userService";
import { toast } from "react-toastify";
import { useState } from "react";
import PasswordInput from "../../../../../components/Input/Password";

const SecurityTab = (props) => {
   const userId = useSelector(selectUserId);
   const [data, setData] = useState({
      id: userId,
      currentPassword: "",
      newPassword: "",
      retypePassword: "",
   });

   const handleChangePassword = async (data) => {
      try {
         if (data.retypePassword === data.newPassword) {
            const response = await changePasswordService(data);
            if (response && response.status === 200) {
               response.data.errCode === 0
                  ? toast.success(response.data.errMessage)
                  : toast.error(response.data.errMessage);
            }
         } else {
            toast.error("Mật khẩu mới không khớp! Vui lòng nhập lại!");
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleOnchange = (e) => {
      const { name, value } = e.target;
      setData((prev) => ({ ...prev, [name]: value }));
   };

   return (
      <div className="security-tab">
         <div className="form-item">
            <label htmlFor="password">Mật khẩu hiện tại</label>
            <PasswordInput
               name="currentPassword"
               type="password"
               className="password"
               value={data.currentPassword}
               onChange={handleOnchange}
            />
         </div>
         <div className="form-item">
            <label htmlFor="password">Mật khẩu mới</label>
            <PasswordInput
               name="newPassword"
               type="password"
               className="password"
               value={data.newPassword}
               onChange={handleOnchange}
            />
         </div>
         <div className="form-item">
            <label htmlFor="password">Xác nhận mật khẩu mới</label>
            <PasswordInput
               name="retypePassword"
               type="password"
               className="password"
               value={data.retypePassword}
               onChange={handleOnchange}
            />
         </div>

         <div className="btn-submit mt-3">
            <Button
               className="btn-beige"
               type="primary"
               label="Cập nhật"
               onClick={() => handleChangePassword(data)}
            />
         </div>
      </div>
   );
};

export default SecurityTab;
