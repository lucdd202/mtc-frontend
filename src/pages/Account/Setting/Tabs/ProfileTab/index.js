import { useSelector } from "react-redux";
import "./index.scss";
import { selectUserId } from "../../../../../stores/slices/authenSlice";
import { useCallback, useEffect, useState } from "react";
import {
   getUserByIdService,
   updateUserService,
} from "../../../../../services/userService";
import { convertImage } from "../../../../../utils/function";
import Loading from "./../../../../../components/Loading/index";
import Button from "./../../../../../components/Button/index";
import { CommonUtils } from "../../../../../utils";

const ProfileTab = (props) => {
   const userId = useSelector(selectUserId);
   const [userDetail, setUserDetail] = useState();
   const [previewImg, setPreviewImg] = useState();

   const handleFetchUserDetail = useCallback(async () => {
      try {
         const response = await getUserByIdService(userId);
         if (response && response.status === 200) {
            setUserDetail(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId]);

   useEffect(() => {
      handleFetchUserDetail();
   }, [handleFetchUserDetail]);

   const handleOnchange = (e) => {
      const { name, value } = e.target;
      setUserDetail((prev) => ({
         ...prev,
         [name]: value,
      }));
   };

   useEffect(() => {
      console.log("userDETAIL", userDetail);
   }, [userDetail]);

   const handleOnChangeImage = async (event) => {
      let data = event.target.files;
      let file = data[0];
      if (file) {
         let base64 = await CommonUtils.getBase64(file);
         let objectUrl = URL.createObjectURL(file);
         setUserDetail((prev) => ({ ...prev, avatar: base64 }));
         setPreviewImg(objectUrl);
      }
   };

   useEffect(() => {
      if (userDetail && userDetail.avatar) {
         setPreviewImg(convertImage(userDetail.avatar));
      }
   }, [userDetail]);

   const handleUpdateUser = async (data) => {
      try {
         const response = await updateUserService(data);
         if (response && response.status === 200) {
            console.log("succeed!");
         }
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <>
         {userDetail ? (
            <div className="profile-tab">
               <div className="user-avatar">
                  <div
                     key={userDetail.avatar}
                     className="avatar-image"
                     style={{
                        backgroundImage: `url(${previewImg})`,
                     }}
                  >
                     <input
                        className="upload-file"
                        type="file"
                        onChange={handleOnChangeImage}
                     />
                  </div>
                  <p>Ấn vào ảnh để thay đổi ảnh đại diện</p>
               </div>

               <hr />
               <div className="form-item">
                  {" "}
                  <label htmlFor="name">Tên hiển thị</label>
                  <input
                     name="name"
                     className="user-name"
                     value={userDetail.name}
                     onChange={handleOnchange}
                  />
               </div>
               <div className="form-item">
                  {" "}
                  <label htmlFor="quote">Giới thiệu ngắn</label>
                  <textarea
                     name="quote"
                     className="quote"
                     value={userDetail.quote}
                     onChange={handleOnchange}
                  />
               </div>
               <div className="form-item">
                  {" "}
                  <label htmlFor="email">Email</label>
                  <input
                     name="email"
                     type="email"
                     className="email"
                     value={userDetail.email}
                     onChange={handleOnchange}
                  />
               </div>

               <div className="btn-submit mt-3">
                  <Button
                     className="btn-beige"
                     type="primary"
                     label="Cập nhật"
                     onClick={() => handleUpdateUser(userDetail)}
                  />
               </div>
            </div>
         ) : (
            <Loading />
         )}
      </>
   );
};

export default ProfileTab;
