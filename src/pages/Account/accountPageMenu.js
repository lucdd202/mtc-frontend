export const accountPageMenu = [
   {
      label: (
         <>
            <i className="fas fa-book mr-2"></i>Tủ truyện
         </>
      ),
      path: "tu-truyen",
   },
   {
      label: (
         <>
            <i className="fas fa-gear mr-2"></i>Cài đặt
         </>
      ),
      path: "cai-dat",
   },
   {
      label: (
         <>
            <i className="fas fa-money-bill-alt mr-2"></i>Mua kẹo
         </>
      ),
      path: "mua-keo",
   },
   {
      label: (
         <>
            <i className="fas fa-bell mr-2"></i>Thông báo
         </>
      ),
      path: "thong-bao",
   },
   {
      label: (
         <>
            <i className="fas fa-comment mr-2"></i>Trợ giúp & Báo lỗi
         </>
      ),
      path: "tro-giup",
   },
];
