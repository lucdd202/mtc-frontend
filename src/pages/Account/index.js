import { Navigate, Route, Routes } from "react-router";
import ListGroup from "../../components/ListGroup";
import { accountPageMenu } from "./accountPageMenu";
import "./index.scss";
import Bookcase from "./Bookcase";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectUserId } from "../../stores/slices/authenSlice";
import Setting from "./Setting";
import NotificationPage from "./Notification";
import Purchase from "./Purchase";

const AccountPage = (props) => {
   const userId = useSelector(selectUserId);

   return (
      <div className="account-page row">
         <div className="col-3">
            <Link className="link-profile" to={`/ho-so/${userId}`}>
               <i className="fas fa-user mr-2"></i>Hồ sơ
            </Link>
            <ListGroup data={accountPageMenu} />
         </div>
         <div className="col-9">
            <Routes path="/tai-khoan" element={<AccountPage />}>
               <Route path="/tu-truyen" element={<Bookcase />} />
               <Route path="/cai-dat" element={<Setting />} />
               <Route path="/thong-bao" element={<NotificationPage />} />
               <Route path="/mua-keo" element={<Purchase />} />
               <Route
                  path="/*"
                  element={<Navigate to="/tai-khoan/tu-truyen" replace />}
               />
            </Routes>
         </div>
      </div>
   );
};

export default AccountPage;
