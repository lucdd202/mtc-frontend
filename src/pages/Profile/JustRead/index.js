import { UserOutlined } from "@ant-design/icons";
import Loading from "../../../components/Loading";
import "./index.scss";
import { Link } from "react-router-dom";
import { convertImage } from "../../../utils/function";
import { SwiperSlide, Swiper } from "swiper/react";
import { EffectCoverflow, Navigation, Pagination } from "swiper/modules";

const JustRead = (props) => {
   const { data } = props;

   return (
      <div className="just-read">
         <h5 className="mb-4">Vừa đọc</h5>
         {data?.length > 0 ? (
            <Swiper
               slidesPerView={1}
               loop={true}
               navigation={true}
               spaceBetween={30}
               keyboard={{
                  enabled: true,
               }}
               pagination={{
                  clickable: true,
               }}
               modules={[EffectCoverflow, Pagination, Navigation]}
               className="mySwiper"
            >
               {data?.map((item, index) => (
                  <SwiperSlide key={index}>
                     <div className="history-slide row">
                        <Link
                           to={`/truyen/${item?.bookId}`}
                           className={`slider-img col-3`}
                        >
                           <img
                              src={convertImage(item?.Book?.image)}
                              style={{
                                 width: "100%",
                                 height: "200px",
                              }}
                              alt=""
                           />
                        </Link>
                        <div className="col-9 book-info">
                           <Link to={`/truyen/${item?.bookId}`}>
                              <h6 className="title">{item?.Book?.name}</h6>
                           </Link>
                           <div className="author">
                              <UserOutlined className="mr-2" />
                              {item?.Book?.BookInfo?.Author.value}
                           </div>
                           <div
                              className="description"
                              dangerouslySetInnerHTML={{
                                 __html: item?.Book?.description,
                              }}
                           ></div>
                        </div>
                     </div>
                  </SwiperSlide>
               ))}
            </Swiper>
         ) : (
            <Loading />
         )}
      </div>
   );
};

export default JustRead;
