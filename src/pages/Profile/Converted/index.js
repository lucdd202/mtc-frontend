import { Fragment } from "react";
import Loading from "../../../components/Loading";
import "./index.scss";
import BookCard from "../../../components/Card/BookCard";

const Converted = (props) => {
   const { data } = props;

   return (
      <div className="converted">
         <h5 className="mb-3">Đã đăng</h5>
         <div className="book-containter">
            {data.length > 0 ? (
               data.map((item, index) => (
                  <Fragment key={index}>
                     <BookCard
                        className="col-12"
                        type="rank"
                        data={item}
                        image={true}
                     />
                     {data[index + 1] && <hr />}
                  </Fragment>
               ))
            ) : (
               <Loading />
            )}
         </div>
      </div>
   );
};

export default Converted;
