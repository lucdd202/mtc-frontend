import { UserOutlined } from "@ant-design/icons";
import { convertImage, convertTime } from "../../../utils/function";
import "./index.scss";
import Loading from "../../../components/Loading";

const AboutUser = (props) => {
   const { data } = props;

   return (
      <div className="about-user">
         {data ? (
            <>
               <div className="row user-info">
                  <img
                     className="user-img col-4"
                     src={convertImage(data.avatar)}
                     width={90}
                     height={90}
                     alt=""
                  />
                  <div className="col-8">
                     <h6>{data?.name}</h6>
                     <p className="created-date">
                        <UserOutlined /> {convertTime(data?.createdAt)}
                     </p>
                  </div>
               </div>

               <div className="user-quote">{data?.quote}</div>
               <div className="user-progress">
                  <table className="table table-border-less">
                     <tbody>
                        <tr>
                           <td className="">ĐÃ ĐỌC</td>
                           <td>
                              <div>
                                 <b>{data?.historyCount}</b> Truyện
                              </div>
                              <div>
                                 <b>{data?.chapterRead}</b> Chương
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td>BÌNH LUẬN</td>
                           <td>
                              <b>{data?.commentCount}</b>
                           </td>
                        </tr>
                        <tr>
                           <td>ĐỀ CỬ</td>
                           <td>
                              <b>{data?.flowerGive}</b>
                           </td>
                        </tr>
                        <tr>
                           <td>THÍCH</td>
                           <td></td>
                        </tr>
                        <tr>
                           <td>CẤT GIỮ</td>
                           <td>
                              <b>{data?.bookmarkCount}</b>
                           </td>
                        </tr>
                     </tbody>
                  </table>
               </div>
            </>
         ) : (
            <Loading />
         )}
      </div>
   );
};

export default AboutUser;
