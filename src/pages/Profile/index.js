import "./index.scss";
import { useParams } from "react-router-dom";
import { getAllHistoriesService } from "../../services/historyService";
import { useEffect, useState } from "react";
import { getUserByIdService } from "../../services/userService";
import JustRead from "./JustRead";
import AboutUser from "./AboutUser";
import Converted from "./Converted";
import { getConvertedBooksService } from "../../services/bookService";

const ProfilePage = (props) => {
   const { userId } = useParams();
   const [histories, setHistories] = useState([]);
   const [userDetail, setUserDetail] = useState();
   const [convertedBooks, setConvertedBooks] = useState([]);

   const handleFetchAllHistories = async (userId, limit) => {
      try {
         const response = await getAllHistoriesService({ userId, limit });

         if (response.status === 200) {
            setHistories(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleGetUserDetail = async (idInput) => {
      try {
         const response = await getUserByIdService(idInput);
         if (response) {
            setUserDetail(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (userId) {
         handleGetUserDetail(userId);
         handleFetchAllHistories(userId, 10);
      }
   }, [userId]);

   const handleGetConvertedBooks = async (idInput) => {
      try {
         const response = await getConvertedBooksService(idInput);
         if (response && response.status === 200) {
            setConvertedBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   console.log(userDetail);

   useEffect(() => {
      if (userDetail && userDetail?.roleId === "ROLE1") {
         handleGetConvertedBooks(userDetail.id);
      }
   }, [userDetail]);

   return (
      <div className="profile-page row">
         <div className="col-8">
            <JustRead data={histories} />
            {convertedBooks.length > 0 && <Converted data={convertedBooks} />}
         </div>
         <div className="col-4">
            <AboutUser data={userDetail} />
         </div>
      </div>
   );
};

export default ProfilePage;
