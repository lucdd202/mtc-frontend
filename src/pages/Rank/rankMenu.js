export const rankCategory = [
   {
      label: "Đọc nhiều",
      path: "VIEW",
   },
   {
      label: "Tặng thưởng",
      path: "CANDY",
   },
   {
      label: "Đề cử",
      path: "FLOWER",
   },
   {
      label: "Yêu thích",
      path: "FAVORITE",
   },
   {
      label: "Thảo luận",
      path: "COMMENT",
   },
];

export const rankTypes = [
   { label: "Theo tuần", value: "WEEK" },
   { label: "Theo tháng", value: "MONTH" },
];
