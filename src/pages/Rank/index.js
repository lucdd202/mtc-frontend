import { useNavigate, useParams } from "react-router-dom";
import { rankCategory, rankTypes } from "./rankMenu";
import { Fragment, useEffect, useState } from "react";
import { getBooksByRankService } from "../../services/bookService";
import BookCard from "../../components/Card/BookCard";
import "./index.scss";
import ListGroup from "../../components/ListGroup";

const RankData = (props) => {
   const [books, setBooks] = useState([]);
   const { type, category } = useParams();

   const handleFetchAllBooks = async (rankType, rankKey) => {
      try {
         let response = await getBooksByRankService({
            rankType,
            rankKey,
         });
         if (response && response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleFetchAllBooks(type, category);
   }, [type, category]);

   return (
      <div className="book-container row">
         {books.map((item, index) => (
            <Fragment key={index}>
               <BookCard
                  className="col-12"
                  type="rank"
                  data={item}
                  image={true}
               />
               {books[index + 1] && <hr />}
            </Fragment>
         ))}
      </div>
   );
};

const RankPage = (props) => {
   const { category } = useParams();
   const navigate = useNavigate();
   const [selectedType, setSelectedType] = useState("Theo tuần");

   const handleTypeChange = (data) => {
      setSelectedType(data.label);
      navigate(`/bang-xep-hang/${data.value}/${category}`);
   };

   return (
      <div className="rank-page row">
         <div className="col-3 rank-category">
            <ListGroup data={rankCategory} />
         </div>
         <div className="col-9 px-4">
            <div className="rank-type mb-2" onChange={handleTypeChange}>
               <div
                  type="button"
                  className={`dropdown-toggle`}
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
               >
                  Xếp hạng: <b>{selectedType}</b>
               </div>
               <div className="dropdown-menu dropdown-menu-right">
                  {rankTypes.map((item, index) => (
                     <div
                        className={`dropdown-item ${
                           item.label === selectedType ? "active" : ""
                        }`}
                        key={index}
                        onClick={() => handleTypeChange(item)}
                     >
                        Xếp hạng: <b>{item.label}</b>
                     </div>
                  ))}
               </div>
            </div>
            <br />
            <RankData />
         </div>
      </div>
   );
};

export default RankPage;
