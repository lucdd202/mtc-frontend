import { sortMenu } from "../../../../utils/constant";
import "./index.scss";

const SortSection = (props) => {
   const { sort, setSort } = props;

   const handleSort = (newKey, newType) => {
      setSort({ key: newKey, type: newType });
   };

   const showLabel = (arr) => {
      let labelToShow = arr[0].label;

      arr.forEach((item) => {
         if (sort.key === item.key && sort.type === item.type) {
            labelToShow = item.label;
         }
      });

      return labelToShow;
   };

   const checkActive = (data) => {
      let isActive = "";

      if (typeof data === "object") {
         if (sort.key === data.key && sort.type === data.type) {
            isActive = "active";
         }
      }
      if (Array.isArray(data)) {
         data.forEach((item) => {
            if (sort.key === item.key && sort.type === item.type) {
               isActive = "active";
            }
         });
      }

      return isActive;
   };

   return (
      <div className="d-flex sort-menu">
         {sortMenu.map((group, groupIndex) => (
            <div
               key={groupIndex}
               className={`sort-item ${Array.isArray(group) ? "drop" : ""}`}
            >
               {Array.isArray(group) ? (
                  <>
                     <div
                        type="button"
                        className={`dropdown-toggle ${checkActive(group)}`}
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                     >
                        {showLabel(group)}
                     </div>
                     <div className="dropdown-menu">
                        {group.map((option, optionIndex) => (
                           <div
                              key={optionIndex}
                              className={`dropdown-item ${checkActive(option)}`}
                              onClick={() =>
                                 handleSort(option.key, option.type)
                              }
                           >
                              {option.label}
                           </div>
                        ))}
                     </div>
                  </>
               ) : (
                  <div
                     onClick={() => handleSort(group.key, group.type)}
                     className={checkActive(group)}
                  >
                     {group.label}
                  </div>
               )}
            </div>
         ))}
      </div>
   );
};

export default SortSection;
