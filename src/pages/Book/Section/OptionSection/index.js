import { Fragment, useCallback, useEffect, useState } from "react";
import "./index.scss";
import {
   convertCategoryService,
   getAllCategoriesService,
} from "../../../../services/categoryService";
import { optionMenu } from "../../../../utils/constant";
import { useSearchParams } from "react-router-dom";

const OptionSection = (props) => {
   const { options, setOptions } = props;
   const [searchParams, setSearchParams] = useSearchParams();
   const keyword = searchParams.get("keyword");
   const genre = searchParams.get("genre");
   const [data, setData] = useState([]);

   const clearKeyword = () => {
      searchParams.delete("keyword");
      setSearchParams(searchParams);
   };

   const handleFetchOptions = useCallback(async () => {
      try {
         const result = optionMenu.map(async (optionItem) => {
            const response = await getAllCategoriesService(optionItem.value);
            if (response.status === 200) {
               const newData = {
                  label: optionItem.label,
                  children: response.data.map((child) => ({
                     ...child,
                     isActive: child.key === genre ? true : false,
                  })),
               };

               setData((prevData) => {
                  if (
                     prevData.some(
                        (dataItem) => dataItem.label === optionItem.label
                     )
                  ) {
                     return prevData;
                  } else {
                     return [...prevData, newData];
                  }
               });
            }
         });
         await Promise.all(result);
      } catch (e) {
         console.log(e);
      }
   }, [options]);

   useEffect(() => {
      handleFetchOptions();
   }, [handleFetchOptions]);

   console.log("data", data);

   const convertGenre = useCallback(async () => {
      try {
         if (genre) {
            const response = await convertCategoryService(genre);
            if (response && response.status === 200) {
               setOptions((prevOptions) => [...prevOptions, response.data]);
            }
         }
      } catch (e) {
         console.error("Error converting genre:", e);
      }
   }, [genre, setOptions]);

   useEffect(() => {
      convertGenre();
   }, [convertGenre]);

   return (
      <div className="option-section">
         <div className="select">
            <h6>Đã chọn</h6>
            <div className="select-container">
               {keyword && (
                  <div className="search-item">
                     Đang tìm: {keyword}{" "}
                     <i
                        className="fas fa-times ml-1"
                        onClick={clearKeyword}
                     ></i>
                  </div>
               )}
               {options.map((item, index) => (
                  <div className="select-item" key={index}>
                     {item.value}
                     <i
                        className="fas fa-times ml-1"
                        onClick={() => {
                           options.splice(index, 1);
                           setOptions([...options]);
                           data.forEach((dataItem, i) => {
                              const childrenIndex = dataItem.children.findIndex(
                                 (child) => child.value === item.value
                              );
                              if (childrenIndex !== -1) {
                                 data[i].children[
                                    childrenIndex
                                 ].isActive = false;
                              }
                           });
                           setData([...data]);
                        }}
                     ></i>
                  </div>
               ))}
            </div>
         </div>
         <hr />
         {data.map((item, i) => (
            <Fragment key={i}>
               <div className="option">
                  <h6>{item.label}</h6>
                  <div className="option-container">
                     {item.children.map((item, j) => (
                        <div
                           className={`option-item ${
                              item?.isActive ? "active" : ""
                           }`}
                           key={j}
                           onClick={() => {
                              data[i].children[j].isActive =
                                 !data[i].children[j].isActive;
                              setData([...data]);
                              if (item.isActive) {
                                 setOptions((prev) => [...prev, item]);
                              } else {
                                 const itemIndex = options.findIndex(
                                    (option) => option.value === item.value
                                 );
                                 if (itemIndex !== -1) {
                                    options.splice(itemIndex, 1);
                                    setOptions([...options]);
                                 }
                              }
                           }}
                        >
                           {item.value}
                        </div>
                     ))}
                  </div>
               </div>
               {data[i + 1] && <hr />}
            </Fragment>
         ))}
      </div>
   );
};

export default OptionSection;
