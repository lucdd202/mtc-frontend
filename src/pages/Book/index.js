import "./index.scss";
import OptionSection from "./Section/OptionSection";
import BookSection from "./../Home/Section/BookSection/index";
import SortSection from "./Section/SortSection";
import { useEffect, useState } from "react";
import { searchBooksService } from "../../services/bookService";
import { useSearchParams } from "react-router-dom";

const BookPage = () => {
   const [books, setBooks] = useState([]);
   const [searchParams, setSearchParams] = useSearchParams();
   const keyword = searchParams.get("keyword");
   const [options, setOptions] = useState([]);
   const [sort, setSort] = useState({
      key: "UPDATED",
      type: "",
   });

   const handleSearchBooks = async (text, options, sortBy) => {
      try {
         const response = await searchBooksService({ text, options, sortBy });
         if (response && response.status === 200) {
            setBooks(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      handleSearchBooks(keyword, options, sort);
   }, [options, keyword, sort]);

   return (
      <div className="book-page row">
         <div className="col-3">
            <OptionSection options={options} setOptions={setOptions} />
         </div>
         <div className="col-9 px-4">
            <SortSection sort={sort} setSort={setSort} />
            <BookSection data={books} />
         </div>
      </div>
   );
};

export default BookPage;
