import { Modal } from "antd";
import guide from "../../../../assets/images/guide.png";
import "./index.scss";

const Guide = (props) => {
   const { isModalOpen, setIsModalOpen } = props;

   return (
      <Modal
         width={500}
         title={"Hướng dẫn"}
         open={isModalOpen}
         footer={false}
         onCancel={() => setIsModalOpen(false)}
      >
         <div className="guide-modal">
            <img src={guide} width="100%" alt="" />
            <div className="guide-close" onClick={() => setIsModalOpen(false)}>
               Tắt
            </div>
         </div>
      </Modal>
   );
};

export default Guide;
