export const colorMenu = [
   {
      backgroundColor: "#f5e4e4",
      color: "#333",
      pageBackground: "#e9d8d8",
      border: "#ebcaca",
   },
   {
      backgroundColor: "#f5ebcd",
      color: "#333",
      pageBackground: "#eae0c3",
      border: "#efdfaf",
   },
   {
      backgroundColor: "#e2eee2",
      color: "#333",
      pageBackground: "#ccd8cc",
      border: "#cbe1cb",
   },
   {
      backgroundColor: "#e1e8e8",
      color: "#333",
      pageBackground: "#ced9d9",
      border: "#cdd9d9",
   },
   {
      backgroundColor: "#eae4d3",
      color: "#333",
      pageBackground: "#e4dece",
      border: "#ded5bb",
   },
   {
      backgroundColor: "#e5e3df",
      color: "#333",
      pageBackground: "#d7d4ce",
      border: "#d5d2cb",
   },
   {
      backgroundColor: "#333",
      color: "#ccc",
      pageBackground: "#222",
      border: "#101010",
   },
   {
      backgroundColor: "#fff",
      color: "#333",
      pageBackground: "#e5e5e5",
      border: "#ededed",
   },
];

export const fontMenu = [
   {
      label: "Palatino Linotype",
      value: "'Palatino Linotype'",
   },
   {
      label: "Source Sans Pro",
      value: "'Source Sans Pro'",
   },
   {
      label: "Segoe UI",
      value: "'Segoe UI'",
   },
   {
      label: "Roboto",
      value: "'Roboto'",
   },
   {
      label: "Patrick Hand",
      value: "'Patrick Hand'",
   },
   {
      label: "Noticia Text",
      value: "'Noticia Text'",
   },
   {
      label: "Times New Roman",
      value: "'Times New Roman'",
   },
   {
      label: "Verdana",
      value: "'Verdana'",
   },
   {
      label: "Tahoma",
      value: "'Tahoma'",
   },
   {
      label: "Arial",
      value: "'Arial'",
   },
];
