import { useCallback, useEffect, useState } from "react";
import "./index.scss";
import { FontSizeOutlined } from "@ant-design/icons";
import Counter from "../../../../components/Counter";
import { colorMenu, fontMenu } from "./configMenu";
import { updateConfigurationService } from "../../../../services/configurationService";
import { useDispatch, useSelector } from "react-redux";
import {
   setDataConfig,
   selectDataConfig,
} from "../../../../stores/slices/configSlice";

const ReadConfig = (props) => {
   const dataConfig = useSelector(selectDataConfig);
   //theme , fontFamily, fontSize , width , lineSpacing
   const [config, setConfig] = useState(dataConfig);
   const dispatch = useDispatch();

   const handleUpdateConfiguration = useCallback(async () => {
      try {
         const response = await updateConfigurationService(config);

         if (response && response.status === 200) {
            dispatch(setDataConfig(response.data));
         }
      } catch (e) {
         console.log(e);
      }
   }, [config, dispatch]);

   useEffect(() => {
      handleUpdateConfiguration();
   }, [handleUpdateConfiguration]);

   return (
      <div className="read-config">
         <h5 className="mb-4">Cài đặt</h5>
         <div className="config-menu">
            <div className="menu-item row mb-3">
               <h6 className="col-5">
                  <i className="fas fa-palette mr-2"></i> Màu nền
               </h6>
               <div className="col-7 color-container row">
                  {colorMenu.map((item, index) => (
                     <div className="color-item col-3 mb-3" key={index}>
                        <div
                           style={{
                              backgroundColor: item.backgroundColor,
                              border: `1px solid ${item.border}`,
                              borderRadius: "50%",
                              height: "38px",
                              width: "38px",
                           }}
                           onClick={() =>
                              setConfig((prev) => ({
                                 ...prev,
                                 theme: index,
                              }))
                           }
                        >
                           {colorMenu[config.theme].backgroundColor ===
                              item.backgroundColor && (
                              <i className="fas fa-check"></i>
                           )}
                        </div>
                     </div>
                  ))}
               </div>
            </div>
            <div className="menu-item row mb-3">
               <h6 className="col-5">
                  <i className="fas fa-font mr-3"></i>
                  Font chữ
               </h6>

               <select
                  className="font-select col-7"
                  value={config.fontFamily ?? fontMenu[0].value}
                  onChange={(e) =>
                     setConfig((prev) => ({
                        ...prev,
                        fontFamily: e.target.value,
                     }))
                  }
               >
                  {fontMenu.map((item, index) => (
                     <option key={index} value={item.value}>
                        {item.label}
                     </option>
                  ))}
               </select>
            </div>
            <div className="menu-item row mb-3">
               <h6 className="col-5">
                  <FontSizeOutlined className="mr-3" />
                  Cỡ chữ
               </h6>
               <div className="col-7 row">
                  <Counter
                     data={config.fontSize}
                     setData={(value) =>
                        setConfig((prev) => ({ ...prev, fontSize: value }))
                     }
                     typeConfig="SIZE"
                  />
               </div>
            </div>
            <div className="menu-item row mb-3">
               <h6 className="col-5">
                  <i className="fas fa-text-width mr-3"></i>
                  Chiều rộng khung
               </h6>
               <div className="col-7 row">
                  <Counter
                     typeConfig="WIDTH"
                     data={config.width}
                     setData={(value) =>
                        setConfig((prev) => ({ ...prev, width: value }))
                     }
                  />
               </div>
            </div>
            <div className=" menu-item row mb-3">
               <h6 className="col-5">
                  <i className="fas fa-text-height mr-3"></i>
                  Giãn dòng
               </h6>
               <div className="col-7 row">
                  <Counter
                     typeConfig="SPACING"
                     data={config.lineSpacing}
                     setData={(value) =>
                        setConfig((prev) => ({
                           ...prev,
                           lineSpacing: value,
                        }))
                     }
                  />
               </div>
            </div>
         </div>
      </div>
   );
};

export default ReadConfig;
