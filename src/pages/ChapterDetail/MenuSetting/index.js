import { useState } from "react";
import { useNavigate, useParams } from "react-router";
import { getAllChaptersService } from "../../../services/chapterService";
import Guide from "./Guide";
import ChapterList from "../../../components/ChapterList";
import ReadConfig from "./ReadConfig";
import { colorMenu } from "./ReadConfig/configMenu";
import { useSelector } from "react-redux";
import { selectDataConfig } from "../../../stores/slices/configSlice";
import "./index.scss";

const MenuSetting = (props) => {
   const { fixed } = props;
   const [chapters, setChapters] = useState();
   const [isModalOpen, setIsModalOpen] = useState(false);
   const navigate = useNavigate();
   const showModal = () => {
      setIsModalOpen(true);
   };
   const { bookId } = useParams();
   const dataConfig = useSelector(selectDataConfig);

   const handleFetchAllChapters = async (idInput) => {
      try {
         const response = await getAllChaptersService(idInput);
         if (response.status === 200) {
            setChapters(response.data);
         }
      } catch (e) {
         console.log("fetch all chapters failed ", e);
      }
   };

   return (
      <>
         <div
            className="menu-setting "
            style={{
               backgroundColor: colorMenu[dataConfig.theme].backgroundColor,
               color: colorMenu[dataConfig.theme].color,
               position: fixed ? "fixed" : "absolute",
            }}
         >
            <div className="setting-item dropleft">
               <div
                  className="dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                  data-bs-auto-close="false"
                  onClick={() => handleFetchAllChapters(bookId)}
               >
                  <i className="fas fa-bars"></i>
               </div>

               <div
                  className="dropdown-menu"
                  onClick={(e) => e.stopPropagation()}
               >
                  <ChapterList type="CHAPTER" data={chapters} />
               </div>
            </div>
            <hr />
            <div className="setting-item dropleft">
               <div
                  className="dropdown-toggle"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
               >
                  <i className="fas fa-cog"></i>
               </div>

               <div
                  className="dropdown-menu"
                  onClick={(e) => e.stopPropagation()}
               >
                  <ReadConfig />
               </div>
            </div>

            <hr />
            <div
               className="setting-item"
               onClick={() => navigate(`/truyen/${bookId}`)}
            >
               <i className="fas fa-arrow-left"></i>
            </div>
            <hr />
            <div className="setting-item">
               <i className="fas fa-info-circle" onClick={showModal}></i>
            </div>
         </div>{" "}
         <Guide isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} />
      </>
   );
};

export default MenuSetting;
