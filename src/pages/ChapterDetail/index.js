import { useNavigate, useParams } from "react-router";
import "./index.scss";
import {
   getAllChaptersService,
   getChapterByNumberService,
} from "../../services/chapterService";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect, useRef, useState } from "react";
import Header from "../../components/Header";
import { selectUserId } from "../../stores/slices/authenSlice";
import CommentTab from "../../pages/BookDetail/Tab/CommentTab";
import { Link } from "react-router-dom";
import { colorMenu } from "./MenuSetting/ReadConfig/configMenu";
import { getConfigurationService } from "../../services/configurationService";
import MenuSetting from "./MenuSetting";
import {
   selectDataConfig,
   setDataConfig,
} from "../../stores/slices/configSlice";
import Loading from "../../components/Loading";
import EndChapter from "./EndChapter";
import { getBookByIdService } from "../../services/bookService";
import moment from "moment";

const ChapterDetailPage = () => {
   const { bookId, chapterNumber } = useParams();
   const [chapterDetail, setChapterDetail] = useState();
   const userId = useSelector(selectUserId);
   const dataConfig = useSelector(selectDataConfig);
   const dispatch = useDispatch();
   const [maxChapter, setMaxChapter] = useState();
   const headerRef = useRef(null);
   const [bookDetail, setBookDetail] = useState();
   const [isFixed, setFixed] = useState(false);
   const navigate = useNavigate();

   const handleScroll = () => {
      const scrollPosition = window.scrollY;

      const headerHeight = headerRef.current
         ? headerRef.current.clientHeight
         : 0;

      setFixed(scrollPosition > headerHeight);
   };

   useEffect(() => {
      if (bookDetail) {
         document.title = `Đọc ${bookDetail.name}`;
      }
   }, [bookDetail]);

   useEffect(() => {
      window.addEventListener("scroll", handleScroll);

      return () => {
         window.removeEventListener("scroll", handleScroll);
      };
   }, []);

   const handleGetBookDetail = useCallback(async () => {
      try {
         const respone = await getBookByIdService(bookId);
         if (respone) {
            setBookDetail(respone.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [bookId]);

   useEffect(() => {
      handleGetBookDetail();
   }, [handleGetBookDetail]);

   const handleSetMaxChapter = useCallback(async () => {
      try {
         const response = await getAllChaptersService(bookId);
         if (response && response.status === 200) {
            setMaxChapter(response.data.length);
         }
      } catch (e) {
         console.log(e);
      }
   }, [bookId]);

   useEffect(() => {
      handleSetMaxChapter();
   }, [handleSetMaxChapter]);

   const handleGetChapterById = async (userId, bookId, chapterNumber) => {
      try {
         const respone = await getChapterByNumberService({
            userId,
            bookId,
            chapterNumber,
         });
         if (respone.status === 200) {
            setChapterDetail(respone.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (
         bookId &&
         chapterNumber &&
         maxChapter &&
         chapterNumber <= maxChapter
      ) {
         handleGetChapterById(userId, bookId, chapterNumber);
      }
   }, [userId, bookId, chapterNumber, maxChapter]);

   const handleGetConfiguration = useCallback(async () => {
      try {
         const response = await getConfigurationService(userId);
         if (response && response.status === 200) {
            if (response.data) {
               dispatch(setDataConfig(response.data));
            } else {
               dispatch(
                  setDataConfig({
                     userId,
                     fontSize: 28,
                     theme: 0,
                     lineSpacing: 150,
                     width: 1000,
                  })
               );
            }
         }
      } catch (e) {
         console.log(e);
      }
   }, [userId, dispatch]);

   useEffect(() => {
      handleGetConfiguration();
   }, [handleGetConfiguration]);

   const contentRef = useRef(null);
   const [wordCount, setWordCount] = useState();

   useEffect(() => {
      if (contentRef.current && chapterDetail) {
         const contentHtml = contentRef.current.innerHTML;
         const textWithoutHtml = contentHtml
            .replace(/<[^>]*>|&nbsp;/g, " ")
            .replace(/\s{2,}/g, " ")
            .replace(/\n/g, " ")
            .trim(); // Loại bỏ thẻ HTML

         setWordCount(textWithoutHtml.split(/\s+/).length); // Đếm số từ
      }
   }, [chapterDetail, dataConfig]);

   const handleKeydown = (e) => {
      if (e.keyCode === 37) {
         if (chapterNumber > 1) {
            navigate(`/truyen/${bookId}/${+chapterNumber - 1}`);
         } else if (chapterNumber === "chuong-cuoi") {
            navigate(`/truyen/${bookId}/${maxChapter}`);
         }
      } else if (e.keyCode === 39) {
         if (+chapterNumber < maxChapter && chapterNumber !== "chuong-cuoi") {
            navigate(`/truyen/${bookId}/${+chapterNumber + 1}`);
         }
         if (+chapterNumber === maxChapter) {
            navigate(`/truyen/${bookId}/chuong-cuoi`);
         }
      }
   };

   return dataConfig ? (
      <div
         className="chapter-detail-page"
         style={{
            backgroundColor: colorMenu[dataConfig.theme].pageBackground,
            color: colorMenu[dataConfig.theme].color,
         }}
         tabIndex={0}
         onKeyDown={(e) => handleKeydown(e)}
      >
         <Header ref={headerRef} />

         <div className="chapter-main">
            <div
               className="chapter-data"
               style={{
                  width: dataConfig.width,

                  backgroundColor: colorMenu[dataConfig.theme].backgroundColor,
               }}
            >
               <div className="chapter-btn">
                  <Link
                     to={
                        chapterNumber === "chuong-cuoi" && maxChapter > 0
                           ? `/truyen/${bookId}/${maxChapter}`
                           : chapterNumber - 1 > 0 &&
                             `/truyen/${bookId}/${+chapterNumber - 1}`
                     }
                     className={`${
                        (chapterNumber - 1 === 0 || maxChapter <= 0) &&
                        "disabled"
                     } btn-item`}
                     style={
                        colorMenu[dataConfig.theme].backgroundColor === "#222"
                           ? {
                                color: "#ccc",
                             }
                           : {
                                color: "#333",
                             }
                     }
                  >
                     <i className="fas fa-arrow-left mx-2"></i>
                     Chương trước
                  </Link>
                  <Link
                     to={
                        chapterNumber !== "chuong-cuoi" &&
                        `/truyen/${bookId}/${
                           +chapterNumber + 1 <= maxChapter
                              ? +chapterNumber + 1
                              : "chuong-cuoi"
                        }`
                     }
                     className={`btn-item ${
                        chapterNumber === "chuong-cuoi" && "disabled"
                     }`}
                     style={
                        colorMenu[dataConfig.theme].backgroundColor === "#222"
                           ? {
                                color: "#ccc",
                             }
                           : {
                                color: "#333",
                             }
                     }
                  >
                     Chương tiếp
                     <i className="fas fa-arrow-right mx-2"></i>
                  </Link>
               </div>
               {chapterDetail &&
               (+chapterDetail.chapterNumber === +chapterNumber ||
                  chapterNumber === "chuong-cuoi") ? (
                  <>
                     <h3 className="chapter-title">
                        {chapterNumber === "chuong-cuoi"
                           ? "Hết rồi làm sao đây !!!"
                           : ` Chương ${chapterDetail?.chapterNumber}:
                        ${chapterDetail?.title}`}
                     </h3>
                     <div className="chapter-info">
                        <span>
                           <i className="fas fa-book"></i>
                           {bookDetail?.name}
                        </span>
                        <span>
                           <i className="fas fa-edit"></i>
                           {bookDetail?.Converter?.name}
                        </span>
                        <span>
                           <i className="fas fa-heart"></i>
                           {wordCount} chữ
                        </span>
                        <span>
                           <i className="fas fa-heart"></i>0 cảm xúc
                        </span>
                        <span>
                           <i className="fas fa-clock"></i>
                           {moment(chapterDetail.createdAt).format(
                              "YYYY-MM-DD HH:mm:ss"
                           )}
                        </span>
                     </div>

                     {chapterNumber <= maxChapter ? (
                        <div
                           className="chapter-content"
                           dangerouslySetInnerHTML={{
                              __html: chapterDetail?.content,
                           }}
                           ref={contentRef}
                           style={{
                              fontSize: dataConfig.fontSize,
                              fontFamily: `${dataConfig.fontFamily},sans-serif`,
                           }}
                        ></div>
                     ) : (
                        <EndChapter
                           theme={colorMenu[dataConfig.theme]}
                           bookDetail={bookDetail}
                        />
                     )}
                  </>
               ) : (
                  <Loading />
               )}
            </div>

            {chapterNumber <= maxChapter && (
               <>
                  {" "}
                  <div
                     className="action-bar"
                     style={{
                        width: dataConfig.width,
                        backgroundColor:
                           colorMenu[dataConfig.theme].backgroundColor,
                     }}
                  >
                     <Link
                        to={
                           chapterNumber === "chuong-cuoi" && maxChapter > 0
                              ? `/truyen/${bookId}/${maxChapter}`
                              : chapterNumber - 1 > 0 &&
                                `/truyen/${bookId}/${+chapterNumber - 1}`
                        }
                        className="btn-item"
                        style={{ color: colorMenu[dataConfig.theme].color }}
                     >
                        <i className="fas fa-arrow-left mx-2"></i>
                        Chương trước
                     </Link>
                     <div className="btn-report">
                        <i className="fas fa-exclamation-triangle"></i>
                     </div>
                     <Link
                        to={
                           chapterNumber !== "chuong-cuoi" &&
                           `/truyen/${bookId}/${
                              +chapterNumber + 1 <= maxChapter
                                 ? +chapterNumber + 1
                                 : "chuong-cuoi"
                           }`
                        }
                        className="btn-item"
                        style={{ color: colorMenu[dataConfig.theme].color }}
                     >
                        Chương tiếp
                        <i className="fas fa-arrow-right mx-2"></i>
                     </Link>
                  </div>
                  <CommentTab
                     style={{
                        width: dataConfig.width,
                        backgroundColor:
                           colorMenu[dataConfig.theme].backgroundColor,
                        borderRadius: "10px",
                     }}
                     bookId={bookId}
                  />
                  <MenuSetting fixed={isFixed} />
               </>
            )}
         </div>
      </div>
   ) : (
      <Loading />
   );
};

export default ChapterDetailPage;
