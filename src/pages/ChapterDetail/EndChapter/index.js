import { Link } from "react-router-dom";
import "./index.scss";

const EndChapter = (props) => {
   const { theme, bookDetail } = props;

   return (
      <div className="end-chapter">
         <div
            className="message"
            style={{ backgroundColor: theme.pageBackground }}
         >
            Bạn vừa đọc hết chương mới nhất của truyện{" "}
            <Link to={`/truyen/${bookDetail?.id}`}>{bookDetail?.name}</Link>,
            không biết nên đọc tiếp truyện gì khác? Đừng lo lắng, chúng tôi xin
            giới thiệu đến bạn một số bộ truyện mà các "đồng đạo" khác đang đọc{" "}
            <Link to={`/truyen/${bookDetail?.id}`}>{bookDetail?.name}</Link>{" "}
            cũng đang tu luyện. Dữ liệu các truyện này được phân tích từ lịch sử
            đọc truyện trên toàn hệ thống, đảm bảo bạn sẽ tìm được bộ truyện ưng
            ý tiếp theo để đọc từ những truyện bên dưới. Nếu bạn chưa đăng nhập
            thì hãy đăng nhập ngay nhé, dữ liệu sẽ chính xác hơn khi có có lịch
            sử đọc truyện của bạn.
         </div>
         <div>Truyện</div>
      </div>
   );
};

export default EndChapter;
