import "./index.scss";
import ChapterList from "../../../../components/ChapterList";

const ChapterTab = (props) => {
   const { data } = props;

   return (
      <div className="chapter-tab">
         <ChapterList type="BOOK" data={data} />
      </div>
   );
};

export default ChapterTab;
