import { useCallback, useEffect, useState } from "react";
import "./index.scss";
import CommentInput from "../../../../components/Input/Comment";
import {
   createNewRatingService,
   getAllRatingsService,
} from "../../../../services/ratingService";
import { useSelector } from "react-redux";
import { selectUserId } from "../../../../stores/slices/authenSlice";
import Comment from "../../../../components/CommentSection";
import { averageRatingPoint, showStar } from "../../../../utils/function";
import Loading from "../../../../components/Loading";

const RatingSlider = (props) => {
   const { label, value, setValue } = props;

   return (
      <div className="row rating-slider">
         <div className="col-3 label">{label}</div>
         <input
            className="col-8 slider"
            type="range"
            id="points"
            name="points"
            min="0"
            max="5"
            step={0.5}
            value={value}
            onChange={(e) => setValue(e.target.value)}
         />
         <div className="col-1 point">{value}</div>
      </div>
   );
};

const RatingTab = (props) => {
   const { data } = props;
   const [ratingArr, setRatingArr] = useState([]);
   const userId = useSelector(selectUserId);
   const [ratingForm, setRatingForm] = useState({
      userId,
      bookId: null,
      characterPoint: 0,
      storyPoint: 0,
      worldPoint: 0,
      content: "",
   });

   useEffect(() => {
      if (data?.id) {
         setRatingForm((prev) => ({ ...prev, bookId: data?.id }));
      }
   }, [data]);

   const onChange = (name, value) => {
      setRatingForm((prev) => ({ ...prev, [name]: value }));
   };

   const handleFetchAllRatings = useCallback(async () => {
      try {
         console.log("bookId", data);
         const response = await getAllRatingsService(data.id);
         if (response && response.status === 200) {
            setRatingArr(response.data);
         }
      } catch (e) {
         console.log("Error from server", e);
      }
   }, [data]);

   useEffect(() => {
      handleFetchAllRatings();
   }, [handleFetchAllRatings]);

   const handlePostRating = async (data) => {
      try {
         if (!data.characterPoint || !data.storyPoint || !data.worldPoint) {
            console.log("vui lòng cho điểm đầy đủ các mục!");
         } else {
            const response = await createNewRatingService(data);
            if (response && response.status === 200) {
               handleFetchAllRatings();
            }
         }
      } catch (e) {
         console.log(e);
      }
   };

   return (
      <div className="rating-tab row">
         <div className="col-8">
            <div className="rating-form">
               <div className="rating-point">
                  <RatingSlider
                     label={"Tính cách nhân vật"}
                     value={ratingForm.characterPoint}
                     setValue={(value) => onChange("characterPoint", value)}
                  />
                  <RatingSlider
                     label={"Nội dung cốt truyện"}
                     value={ratingForm.storyPoint}
                     setValue={(value) => onChange("storyPoint", value)}
                  />
                  <RatingSlider
                     label={"Bối cảnh thế giới"}
                     value={ratingForm.worldPoint}
                     setValue={(value) => onChange("worldPoint", value)}
                  />
               </div>
               <div className="rating-content row">
                  <CommentInput
                     type="rating"
                     value={ratingForm.content}
                     onChange={(value) => onChange("content", value)}
                     onClick={() => handlePostRating(ratingForm)}
                  />
               </div>
            </div>
            <div className="rating-container">
               {ratingArr.length > 0 ? (
                  ratingArr.map((item, index) => (
                     <Comment
                        replyToId={item.id}
                        fetchAllData={handleFetchAllRatings}
                        data={item}
                        key={index}
                        type="rating"
                     />
                  ))
               ) : (
                  <Loading />
               )}
            </div>
         </div>
         <div className="col-4">
            <div className="satistic">
               <div className="row satistic-row">
                  <h5 className="col-7 satistic-item">
                     Đã có {data?.Ratings?.length} đánh giá
                  </h5>
                  <div className="col-4 satistic-item">
                     {showStar(averageRatingPoint(data?.Ratings, "point"))}
                  </div>
                  <div className="col-1 satistic-item">
                     {averageRatingPoint(data?.Ratings, "point")}
                  </div>
               </div>

               <hr />
               <div className="row satistic-row">
                  <div className="col-7 satistic-item">Tính cách nhân vật</div>
                  <div className="col-4 satistic-item">
                     {showStar(
                        averageRatingPoint(data?.Ratings, "characterPoint")
                     )}
                  </div>
                  <div className="col-1 satistic-item">
                     {averageRatingPoint(data?.Ratings, "characterPoint")}
                  </div>
               </div>
               <div className="row satistic-row">
                  <div className="col-7 satistic-item">Nội dung cốt truyện</div>
                  <div className="col-4 satistic-item">
                     {showStar(averageRatingPoint(data?.Ratings, "storyPoint"))}
                  </div>
                  <div className="col-1 satistic-item">
                     {averageRatingPoint(data?.Ratings, "storyPoint")}
                  </div>
               </div>
               <div className="row satistic-row">
                  <div className="col-7 satistic-item">Bối cảnh thế giới</div>
                  <div className="col-4 satistic-item">
                     {showStar(averageRatingPoint(data?.Ratings, "worldPoint"))}
                  </div>
                  <div className="col-1 satistic-item">
                     {averageRatingPoint(data?.Ratings, "worldPoint")}
                  </div>
               </div>
            </div>
            <div className="attention">
               <h6>Lưu ý khi đánh giá </h6>
               <ol>
                  <li>Không được dẫn link hoặc nhắc đến website khác</li>
                  <li>
                     Không được có những từ ngữ gay gắt, đả kích, xúc phạm người
                     khác
                  </li>
                  <li>
                     Đánh giá hoặc bình luận không liên quan tới truyện sẽ bị
                     xóa
                  </li>
                  <li>
                     Đánh giá hoặc bình luận chê truyện một cách chung chung
                     không mang lại giá trị cho người đọc sẽ bị xóa
                  </li>
                  <li>
                     Đánh giá có điểm số sai lệch với nội dung sẽ bị xóa Vui
                     lòng xem và tuân theo đầy đủ các quy định tại Điều Khoản
                     Dịch Vụ khi sử dụng website
                  </li>
               </ol>
            </div>
         </div>
      </div>
   );
};

export default RatingTab;
