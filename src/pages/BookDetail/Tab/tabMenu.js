import IntroduceTab from "./IntroduceTab";
import CommentTab from "./CommentTab";
import RatingTab from "./RatingTab";
import ChapterTab from "./ChapterTab";

export const tabMenu = (data) => {
   return [
      {
         key: "1",
         label: "Giới thiệu",
         children: <IntroduceTab data={data} />,
      },
      {
         key: "2",
         label: (
            <div>
               Đánh giá
               <span
                  className="px-3"
                  style={{
                     backgroundColor: "#f7f5f0",
                     borderRadius: "30px",
                     fontSize: "14px",
                     fontWeight: "300",
                  }}
               >
                  {data?.Ratings?.length}
               </span>
            </div>
         ),
         children: <RatingTab data={data} />,
      },
      {
         key: "3",
         label: (
            <div>
               D.s chương{" "}
               <span
                  className="px-3"
                  style={{
                     backgroundColor: "#f7f5f0",
                     borderRadius: "30px",
                     fontSize: "14px",
                     fontWeight: "300",
                  }}
               >
                  {data?.Chapters?.length}
               </span>
            </div>
         ),
         children: <ChapterTab data={data?.Chapters} />,
      },
      {
         key: "4",
         label: (
            <div>
               Bình luận
               <span
                  className="px-3"
                  style={{
                     backgroundColor: "#f7f5f0",
                     borderRadius: "30px",
                     fontSize: "14px",
                     fontWeight: "300",
                  }}
               >
                  {data?.Comments?.length}
               </span>
            </div>
         ),
         children: <CommentTab bookId={data?.id} />,
      },
      {
         key: "5",
         label: "Hâm mộ",
         children: "Content of Tab Pane 3",
      },
   ];
};
