import { useSelector } from "react-redux";
import Banner from "../../../../components/Banner";
import { selectUserId } from "../../../../stores/slices/authenSlice";
import { useCallback, useEffect, useState } from "react";
import {
   addNewCommentService,
   getAllCommentsService,
} from "../../../../services/commentService";
import CommentInput from "../../../../components/Input/Comment";
import Comment from "../../../../components/CommentSection";
import "./index.scss";
import Loading from "../../../../components/Loading";
import selectSVG from "../../../../assets/images/select.svg";

const CommentTab = (props) => {
   const { bookId, style } = props;
   const userId = useSelector(selectUserId);
   const [comments, setComments] = useState([]);
   const [commentField, setCommentField] = useState({
      bookId: "",
      userId: "",
      content: "",
      replyToId: "",
   });

   useEffect(() => {
      if (userId && bookId) {
         setCommentField((prev) => ({
            ...prev,
            bookId: bookId,
            userId: userId,
         }));
      }
   }, [userId, bookId]);

   const handleFetchAllComments = useCallback(async () => {
      try {
         const response = await getAllCommentsService(bookId);
         if (response.status === 200) {
            setComments(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [bookId]);

   useEffect(() => {
      handleFetchAllComments();
   }, [handleFetchAllComments]);

   const handleSendComment = async (sendData) => {
      try {
         const response = await addNewCommentService(sendData);
         if (response.status === 200) {
            handleFetchAllComments(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const btnSendClicked = (dataSend) => {
      handleSendComment(dataSend);
      setCommentField((prev) => ({
         ...prev,
         content: "",
      }));
   };

   return (
      <div
         className="row pt-3"
         style={style}
         onKeyDown={(e) => e.stopPropagation()}
         tabIndex={0}
      >
         <div className="col-8 comment">
            <div className="d-flex">
               <h5>{comments.length} bình luận</h5>
               <select style={{ backgroundImage: `url(${selectSVG})` }}>
                  <option>Mới nhất</option>
                  <option>Cũ nhất</option>
                  <option>Lượt thích</option>
               </select>
            </div>
            <CommentInput
               className="mt-3"
               value={commentField.content}
               onChange={(value) =>
                  setCommentField((prev) => ({ ...prev, content: value }))
               }
               onClick={() => btnSendClicked(commentField)}
            />
            <hr></hr>
            {comments.length > 0 ? (
               comments.map((item, index) => (
                  <Comment
                     replyToId={item.id}
                     fetchAllData={handleFetchAllComments}
                     data={item}
                     key={index}
                  />
               ))
            ) : (
               <div className="mt-5">
                  <Loading />
               </div>
            )}
         </div>

         <div className="col-4">
            <Banner number={4} />
         </div>
      </div>
   );
};

export default CommentTab;
