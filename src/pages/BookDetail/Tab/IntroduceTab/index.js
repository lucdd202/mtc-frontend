import ConverterInfo from "../../Section/ConverterSection";

const IntroduceTab = (props) => {
   const { data } = props;

   return (
      <div className="row description">
         <div className="col-8">
            <div
               dangerouslySetInnerHTML={{
                  __html: data?.description,
               }}
            ></div>
            <div></div>
         </div>
         <div className="col-4">
            <ConverterInfo data={data?.converterId} />
            <div className="my-4">
               <h5>Cùng tác giả</h5>
               <hr />
            </div>
         </div>
      </div>
   );
};

export default IntroduceTab;
