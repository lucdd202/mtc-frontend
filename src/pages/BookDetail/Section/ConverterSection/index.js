import { useDispatch, useSelector } from "react-redux";
import { getUserByIdService } from "../../../../services/userService";
import {
   getUserById,
   selectUserDetail,
} from "../../../../stores/slices/userSlice";
import { useCallback, useEffect, useState } from "react";
import "./index.scss";
import { convertImage } from "../../../../utils/function";
import NewBookSection from "../../../Home/Section/NewBookSection";

const ConverterSection = (props) => {
   const { converterId } = props;
   const dispatch = useDispatch();
   const converterDetail = useSelector(selectUserDetail);
   const [imgURL, setImgURL] = useState("");

   const handleGetConverterDetail = useCallback(async () => {
      try {
         const response = await getUserByIdService(converterId);
         if (response.status === 200) {
            dispatch(getUserById(response.data));
         }
      } catch (e) {
         console.log(e);
      }
   }, [converterId, dispatch]);

   useEffect(() => {
      handleGetConverterDetail();
   }, [handleGetConverterDetail]);

   useEffect(() => {
      if (converterDetail?.avatar) {
         setImgURL(convertImage(converterDetail?.avatar));
      }
   }, [converterDetail]);

   return (
      <div className="converter-section">
         <img src={imgURL} className="converter-avatar" alt="" />
         <h5 className="converter-name my-4">{converterDetail?.name}</h5>
         <div className="converter-info row">
            <div className="converter-item col-4">
               <i className="fas fa-book item-icon"></i>
               <div>Số truyện</div>
               <h5> {converterDetail?.convertedCount}</h5>
            </div>
            <div className="converter-item col-4">
               <i className="fas fa-layer-group item-icon"></i>
               <div>Số chương</div>
               <h5>174,3k</h5>
            </div>
         </div>

         <NewBookSection type="converter" />
      </div>
   );
};

export default ConverterSection;
