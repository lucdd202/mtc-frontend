import { Link, useNavigate, useParams } from "react-router-dom";
import "./index.scss";
import { useSelector } from "react-redux";
import { countWeeklyChaptersService } from "../../../../services/chapterService";
import { useEffect, useState } from "react";
import { selectUserId } from "../../../../stores/slices/authenSlice";
import { giveFlowerService } from "../../../../services/userService";
import { toast } from "react-toastify";
import {
   addRemoveBookmarkService,
   checkIsBookmarkedService,
} from "../../../../services/bookmarkService";
import { getHistoryByIdService } from "../../../../services/historyService";
import {
   averageRatingPoint,
   convertImage,
   showStar,
} from "../../../../utils/function";
import flower from "../../../../assets/images/flower.svg";
import BookInfoList from "../../../../components/BookInfoList";
import Button from "../../../../components/Button";

const BookInfoSection = (props) => {
   const { bookDetail, fetchBookDetail } = props;
   const { bookId } = useParams();
   const userId = useSelector(selectUserId);
   const [isBookmarked, setBookmarked] = useState(false);
   const [historyDetail, setHistoryDetail] = useState();
   const [count, setCount] = useState();
   const [image, setImage] = useState();
   const navigate = useNavigate();

   const handleGiveFlower = async (data) => {
      try {
         const response = await giveFlowerService(data);
         if (response.status === 200) {
            if (response.data.code === 0) {
               toast.success(response.data.message);
            } else {
               toast.error(response.data.message);
            }
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleUpdateBookmark = async (data) => {
      try {
         const response = await addRemoveBookmarkService(data);
         if (response.status === 200) {
            handleCheckIsBookmarked(data);
            fetchBookDetail(bookId);
         }
      } catch (e) {
         console.log(e);
      }
   };
   const handleGetHistoryById = async (userId, bookId) => {
      try {
         const response = await getHistoryByIdService({ userId, bookId });
         if (response.status === 200) {
            setHistoryDetail(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const handleCheckIsBookmarked = async (data) => {
      try {
         const response = await checkIsBookmarkedService(data);
         if (response.status === 200) {
            setBookmarked(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   useEffect(() => {
      if (userId && bookId) {
         handleCheckIsBookmarked({ bookId: +bookId, userId: userId });
         handleGetHistoryById(userId, bookId);
      }
   }, [bookId, userId]);

   useEffect(() => {
      if (bookDetail?.image) {
         setImage(convertImage(bookDetail?.image));
      }
   }, [bookDetail]);

   const handleCountWeeklyChapter = async (idInput) => {
      try {
         const response = await countWeeklyChaptersService(idInput);
         if (response.status === 200) {
            setCount(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   };

   const getTotalView = (arr) => {
      const result = arr.find(
         (item) => item.key === "VIEW" && item.type === "TOTAL"
      );
      return result ? result.count : 0;
   };

   useEffect(() => {
      if (bookId) {
         handleCountWeeklyChapter(bookId);
      }
   }, [bookId]);

   const btnReadClick = () => {
      if (bookDetail && bookDetail.Chapters.length > 0) {
         navigate(`/truyen/${bookId}/${historyDetail?.readAt ?? 1}`);
      } else {
         navigate(`/truyen/${bookId}/chuong-cuoi`);
      }
   };

   return (
      <div className="book-info-section row">
         <div
            className="book-image col-3"
            style={{
               backgroundImage: `url(${image})`,
            }}
         ></div>
         <div className="book-info col-9">
            <div className="title">
               <h4>{bookDetail?.name}</h4>
               <Link to="/">
                  <i
                     style={{
                        fontSize: "10px",
                        marginLeft: "5px",
                        cursor: "pointer",
                        color: "#666",
                     }}
                     className="fas fa-flag"
                  ></i>
               </Link>
            </div>

            <BookInfoList data={bookDetail?.BookInfo} />

            <div className="book-data">
               <div className="data-item chapter">
                  <div className="number">{bookDetail?.Chapters?.length}</div>
                  <div className="label">Chương</div>
               </div>
               <div className="data-item chapter">
                  <div className="number">{count}</div>
                  <div className="label">Chương/Tuần</div>
               </div>
               <div className="data-item chapter">
                  <div className="number">
                     {getTotalView(bookDetail?.Analytics)}
                  </div>
                  <div className="label">Lượt đọc</div>
               </div>
               <div className="data-item chapter">
                  <div className="number">{bookDetail?.Bookmarks?.length}</div>
                  <div className="label">Cất giữ</div>
               </div>
            </div>
            <div className="rating">
               <div className="rating-item">
                  {showStar(averageRatingPoint(bookDetail?.Ratings, "point"))}
               </div>
               <div className="rating-item">
                  {`${averageRatingPoint(bookDetail?.Ratings, "point")}/5 
                  (${bookDetail?.Ratings?.length} đánh giá )`}
               </div>
            </div>
            <div className="btn-container">
               <Button
                  onClick={btnReadClick}
                  className="btn-red"
                  type="primary"
                  label="Đọc truyện"
                  leftIcon={<i className="fas fa-glasses"></i>}
               />

               <Button
                  className="btn-gray"
                  onClick={() => handleUpdateBookmark({ bookId, userId })}
                  label={isBookmarked ? "Đã lưu" : "Đánh dấu"}
                  type={"outline"}
                  leftIcon={
                     isBookmarked ? (
                        <i className="fas fa-check mr-3"></i>
                     ) : (
                        <i className="far fa-bookmark mr-3"></i>
                     )
                  }
               />

               <Button
                  label="Đề cử"
                  className="btn-beige"
                  onClick={() => handleGiveFlower({ userId, bookId })}
                  type={"outline"}
                  leftIcon={
                     <div
                        style={{
                           background: `url(${flower}) center center no-repeat`,
                           height: "20px",
                           width: "20px",
                        }}
                     ></div>
                  }
               />
            </div>
         </div>
      </div>
   );
};

export default BookInfoSection;
