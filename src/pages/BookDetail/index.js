import "./index.scss";
import { useLocation, useParams } from "react-router";
import { useCallback, useEffect, useState } from "react";
import { Tabs } from "antd";
import { tabMenu } from "./Tab/tabMenu";
import BookInfoSection from "./Section/BookInfoSection";
import { getBookByIdService } from "../../services/bookService";
import Loading from "../../components/Loading";

const BookDetailPage = (props) => {
   const [bookDetail, setBookDetail] = useState();
   const location = useLocation();
   const { bookId } = useParams();

   useEffect(() => {
      if (bookDetail) {
         document.title = bookDetail?.name;
      }
   }, [location.pathname, bookDetail]);

   const fetchBookDetail = useCallback(async () => {
      try {
         const response = await getBookByIdService(bookId);
         if (response.status === 200) {
            setBookDetail(response.data);
         }
      } catch (e) {
         console.log(e);
      }
   }, [bookId]);

   useEffect(() => {
      fetchBookDetail();
   }, [fetchBookDetail]);

   return (
      <div className="book-detail-page">
         {!bookDetail ? (
            <Loading />
         ) : (
            <>
               <BookInfoSection
                  bookDetail={bookDetail}
                  fetchBookDetail={fetchBookDetail}
               />
               <Tabs
                  className="book-tab"
                  defaultActiveKey="1"
                  items={tabMenu(bookDetail)}
               />
            </>
         )}
      </div>
   );
};

export default BookDetailPage;
