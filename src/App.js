import "./App.scss";
import { Route, Routes } from "react-router-dom";
import { AdminPage, ChapterDetailPage } from "./pages";
import { PrivateRoute } from "./hoc/authentication";
import DefaultLayout from "./components/DefaultLayout";

function App() {
   return (
      <Routes className="App">
         <Route path="/*" element={<DefaultLayout />} />
         <Route
            path="/admin/*"
            element={<PrivateRoute component={AdminPage} />}
         />
         <Route
            path="/truyen/:bookId/:chapterNumber"
            element={<ChapterDetailPage />}
         />
      </Routes>
   );
}

export default App;
