import { Buffer } from "buffer";

export const convertTime = (timeInput) => {
   const postTime = new Date(timeInput);
   const present = new Date();
   const timeSpace = present - postTime;
   const time = timeSpace / 1000;

   const minutesInHour = 60;
   const secondsInMinute = 60;
   const hoursInDay = 24;
   const secondsInHour = secondsInMinute * minutesInHour;
   const secondsInDay = secondsInHour * hoursInDay;

   let timeToShow = "";

   if (time < 60) {
      timeToShow = "vừa xong";
   } else if (time < secondsInHour) {
      timeToShow = Math.floor(time / 60) + " phút trước";
   } else if (time < secondsInDay) {
      timeToShow = Math.floor(time / secondsInHour) + " giờ trước";
   } else if (time < secondsInDay * 30) {
      timeToShow = Math.floor(time / secondsInDay) + " ngày trước";
   } else if (time < secondsInDay * 365) {
      timeToShow = Math.floor(time / (secondsInDay * 30)) + " tháng trước";
   } else {
      timeToShow = Math.floor(time / (secondsInDay * 365)) + " năm trước";
   }

   return timeToShow;
};

export const convertImage = (image) => {
   return Buffer.from(image, "base64").toString("binary");
};

export const showStar = (point) => {
   const fullStar = Math.floor(point);
   const halfStar = point % 1 !== 0 ? 1 : 0;
   const emptyStar = 5 - fullStar - halfStar;

   const mapStar = (value, type) => {
      const stars = [];
      for (let i = 0; i < value; i++) {
         stars.push(
            <i
               key={i}
               className={
                  type === "full"
                     ? `fas fa-star`
                     : type === "half"
                     ? `fas fa-star-half-alt`
                     : "far fa-star"
               }
               style={{ color: "#ffc000" }}
            ></i>
         );
      }
      return stars;
   };

   return (
      <>
         {mapStar(fullStar, "full")}
         {mapStar(halfStar, "half")}
         {mapStar(emptyStar, "empty")}
      </>
   );
};

export const averageRatingPoint = (arr, type) => {
   let result = 0;
   for (let i = 0; i < arr?.length; i++) {
      result += arr[i][type];
   }
   return (result / arr?.length).toFixed(2);
};
