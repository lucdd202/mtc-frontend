export const categoryMenu = [
   {
      value: "ALL",
      label: "Tất cả",
   },
   {
      value: "ROLE",
      label: "Phân quyền",
   },
   {
      value: "AUTHOR",
      label: "Tác giả",
   },
   {
      value: "POV",
      label: "Góc nhìn nhân vật",
   },
   {
      value: "WORLD",
      label: "Bối cảnh thế giới",
   },
   {
      value: "CHARACTER",
      label: "Tính cách nhân vật",
   },
   {
      value: "STATUS",
      label: "Tình trạng",
   },
   {
      value: "FEATURE",
      label: "Lưu phái",
   },
   {
      value: "GENRE",
      label: "Thể loại",
   },
];

export const rankMenu = [
   {
      path: "/bang-xep-hang/WEEK/VIEW",
      label: "Đọc nhiều",
   },
   {
      path: "/bang-xep-hang/WEEK/CANDY",
      label: "Tặng thưởng",
   },
   {
      path: "/bang-xep-hang/WEEK/FLOWER",
      label: "Đề cử",
   },

   {
      path: "/bang-xep-hang/WEEK/FAVORITE",
      label: "Yêu thích",
   },
   {
      path: "/bang-xep-hang/WEEK/COMMENT",
      label: "Thảo luận",
   },
];

export const optionMenu = [
   {
      value: "GENRE",
      label: "Thể loại",
   },
   {
      value: "CHARACTER",
      label: "Tính cách nhân vật",
   },
   {
      value: "STATUS",
      label: "Tình trạng",
   },
   {
      value: "WORLD",
      label: "Bối cảnh thế giới",
   },
   {
      value: "FEATURE",
      label: "Lưu phái",
   },
   {
      value: "POV",
      label: "Góc nhìn nhân vật",
   },
];

export const sortMenu = [
   [
      { key: "UPDATED", type: "", label: "Mới cập nhật" },
      { key: "CREATED", type: "", label: "Mới đăng" },
   ],
   [
      { key: "VIEW", type: "WEEK", label: "Lượt đọc tuần" },
      { key: "VIEW", type: "MONTH", label: "Lượt đọc tháng" },
      { key: "VIEW", type: "TOTAL", label: "Lượt đọc tổng" },
   ],
   [
      { key: "RATING", type: "COUNT", label: "Lượt đánh giá" },
      { key: "RATING", type: "POINT", label: "Điểm đánh giá" },
   ],
   { key: "BOOKMARK", type: "", label: "Cất giữ" },
   { key: "FAVORITE", type: "", label: "Yêu thích" },
   [
      { key: "FLOWER", type: "WEEK", label: "Đề cử tuần" },
      { key: "FLOWER", type: "MONTH", label: "Đề cử tháng" },
      { key: "FLOWER", type: "TOTAL", label: "Đề cử tổng" },
   ],
   { key: "COMMENT", type: "", label: "Bình luận" },
   { key: "CHAPTER", type: "", label: "Số chương" },
];
